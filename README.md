![alt text][logo]

[logo]: Initial-Data/PresentationImages/HeaderReadme.jpg "TelerikAcademy"

# SafetyCar
**The Task**

The goal of the project was to develop a SAFETY CAR web application. It should allow simulation of amount and request insurance policies online. The application functionalities should have been related to three different type of users: public, private and administrators.

The public part of application should have been visible without authentication. They should provide the following functionalities:

- Simulation of car insurance offer based on some input criteria

- Creation of a new account and log-in functionality

The private part of application should have been for registered users only and should've been accessible after successful login. The main functionalities provided for this area are:

- Send request for approval of offer (request policy)

- List history of user’s requests

The administrators of the system should have permission to manage all major information objects in the system. The main functionalities provided for this area are:

- List of requests created in the system

- Approval or rejection of the requests

#
**UI Showcase**
Index page where you can learn more about Safety Car and calculate the estimate price for your car insurance policy quickly from the Policy Calculator page. Here you must enter your car brand and model, car first registration date, your birthday so that we can calculate your age and information about if you had any car accidents in previously years.
![Index](Initial-Data/PresentationImages/IndexPage001.jpg)
After submitting the form your must login or register, so that you can view the estimate price for your policy. After registration you will receive a verification token on your email to confirm that it is you. You must confirm your registration in 24 hours or the link will expire. Then you will need to request a new one.
![Index](Initial-Data/PresentationImages/Login002.jpg)
![Index](Initial-Data/PresentationImages/Register003.jpg)
After successful registration and login you will be redirected to a page where you should add some more information about you and your car. Like when would you like the policy to be active, how would you like to pay- deferred payment or one time payment, and also upload a copy of your car registration certificate.
![Index](Initial-Data/PresentationImages/CreatePolicy004.png)
![Index](Initial-Data/PresentationImages/ChoosePlan005.jpg)
After filling out all the required information you need to confirm your order.
![Index](Initial-Data/PresentationImages/ConfirmYourPolicy006.jpg)
You can also download a copy of your policy as PDF.
![Index](Initial-Data/PresentationImages/PdfInvoice007.jpg)
After submitting your query. Our agents will review your policy and you will receive an email when your policy is accepted or in some cases rejected.
You also have a profile page where you can edit your profile details and track your cars and policies.
![Index](Initial-Data/PresentationImages/ProfilePage008.jpg)
![Index](Initial-Data/PresentationImages/ProfilePageCars010.png)
Agents are allowed to review, accept, reject and delete policies from their agent panel. They can also delete client profiles if they think it's appropriate.
![Index](Initial-Data/PresentationImages/AgentProfile011.png)
![Index](Initial-Data/PresentationImages/AgentProfile012.png)
#
**Technologies Used**

**Back-end:**
 - Java 
 - Spring Boot
 - Spring Security 
 - Spring MVC
 - Spring Session Management
 - Gradle
 - REST
 - Hibernate
 - MySQL/MariaDB
 - Thymeleaf
 
**Front-end:**
 - HTML
 - CSS
 - Java-Script
 - AJAX
 
 #
**Partners**
 
![Footer](Initial-Data/PresentationImages/FooterReadme.jpg)