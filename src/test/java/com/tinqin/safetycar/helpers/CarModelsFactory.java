package com.tinqin.safetycar.helpers;

import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;

public class CarModelsFactory {

    public static CarModel createModel(){
        CarBrand brand=new CarBrand();
        brand.setBrand("Volvo");
        brand.setId(1);
        CarModel model=new CarModel();
        model.setId(2);
        model.setBrand(brand);
        model.setModel("XC70");
        return model;

    }
}
