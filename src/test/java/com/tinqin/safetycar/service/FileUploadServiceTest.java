package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CreatePolicyDto;
import com.tinqin.safetycar.services.FileUploadService;
import com.tinqin.safetycar.services.contracts.VehiclePhotoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@ExtendWith(MockitoExtension.class)
public class FileUploadServiceTest {

    @InjectMocks
    private FileUploadService fileUploadService;

    @Mock
    private VehiclePhotoService vehiclePhotoService;

    @Mock
    private MultipartFile multipartFile;



//    @Test
//    public void saveFile_ShouldSaveFile() throws IOException {
//        //Arrange
//        InputStream inputStream = multipartFile.getInputStream();
//
//        //Act
//        FileUploadService.saveFile("upload", "filename", multipartFile);
//        //Assert
//
//        Mockito.verify
//                (Paths.copy(inputStream, "upload", StandardCopyOption.REPLACE_EXISTING),
//                        Mockito.times(1));
//    }

//    @Test
//    public void savePhotoToFileSystem_ShouldSavePhoto() throws IOException {
//        //Arrange
//        CreatePolicyDto createPolicyDto = new CreatePolicyDto();
//        //Act
//        fileUploadService.savePhotoToFileSystem(createPolicyDto,multipartFile);
//        //Assert
//
//        Mockito.verify(FileUploadService, Mockito.times(1))
//                .saveFile("upload", "filename", multipartFile);
//
//
//    }



}
