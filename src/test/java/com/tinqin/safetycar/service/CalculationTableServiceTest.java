package com.tinqin.safetycar.service;

import com.tinqin.safetycar.models.CalculationTable;
import com.tinqin.safetycar.models.CreatePolicyDto;
import com.tinqin.safetycar.repositories.contracts.CalculationTableRepository;
import com.tinqin.safetycar.services.CalculationTableServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.CalculationTableFactory.createPolicy;
import static com.tinqin.safetycar.helpers.CalculationTableFactory.createTable;
import static com.tinqin.safetycar.services.CalculationTableServiceImpl.ACCIDENT_COEFFICIENT;
import static com.tinqin.safetycar.services.CalculationTableServiceImpl.TAX_PERCENT;

@ExtendWith(MockitoExtension.class)
public class CalculationTableServiceTest {

    @InjectMocks
    CalculationTableServiceImpl calculationTableService;

    @Mock
    CalculationTableRepository mockCalculationTableRepository;

    private List<CalculationTable> testList;
    private CalculationTable testTable;
    private CreatePolicyDto testPolicy;

    @BeforeEach
    public void setup() {
        testPolicy = createPolicy();
        testTable = createTable();
        testList = new ArrayList<>();
    }

    @Test
    public void getAll_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockCalculationTableRepository.getAll()).thenReturn(testList);
        //Act
        List<CalculationTable> actual = new ArrayList<>(calculationTableService.getAll());
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).getAll();

        Assertions.assertEquals(actual, testList);
    }

    @Test
    public void create_ShouldCreate() {
        //Arrange
        Mockito.doNothing().when(mockCalculationTableRepository).create(testTable);
        //Act
        calculationTableService.create(testTable);
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).create(testTable);

    }

    @Test
    public void update_ShouldUpdate() {
        //Arrange
        Mockito.doNothing().when(mockCalculationTableRepository).update(testTable);
        //Act
        calculationTableService.update(testTable);
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).update(testTable);

    }

    @Test
    public void delete_ShouldDelete() {
        //Arrange
        Mockito.doNothing().when(mockCalculationTableRepository).delete(Mockito.anyInt());
        //Act
        calculationTableService.delete(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }

    @Test
    public void getById_ShouldReturn_TableById() {
        //Arrange
        Mockito.when(mockCalculationTableRepository.getById(Mockito.anyInt()))
                .thenReturn(testTable);
        //Act
        calculationTableService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void calculateNetPremium_ShouldReturn_NetPremium() {
        double baseAmount = mockCalculationTableRepository.calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
        //Arrange
        Mockito.when(baseAmount).thenReturn(baseAmount);

        //Act
        calculationTableService.calculateNetPremium(testPolicy);

        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
    }


    @Test
    public void calculateNetPremium_ShouldReturn_NetPremiumWhenAgeIsNull() {
        testPolicy.setDriverBirthday(null);
        double baseAmount = mockCalculationTableRepository.calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
        //Arrange
        Mockito.when(baseAmount).thenReturn(baseAmount);

        //Act
        calculationTableService.calculateNetPremium(testPolicy);

        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
    }



    @Test
    public void reCalculateIfProPlan_ShouldReturn_NetPremiumWhenIsProPlan() {
        double baseAmount = mockCalculationTableRepository.calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
        //Arrange
        Mockito.when(baseAmount).thenReturn(baseAmount);
        //Act
        calculationTableService.reCalculateIfProPlan(testPolicy);
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
    }


    @Test
    public void calculateProPlan_ShouldReturn_NetPremiumWithProPlanCoefficient() {
        double baseAmount = mockCalculationTableRepository.calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
        //Arrange
        Mockito.when(baseAmount).thenReturn(baseAmount);
        //Act
        calculationTableService.calculateProPlan(testPolicy);
        //Assert
        Mockito.verify(mockCalculationTableRepository,
                Mockito.times(1)).calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void calculateTaxes_ShouldReturn_NetPremiumWhenTaxPercent() {
        double netPremium=500.00;
        //Arrange, Act
        calculationTableService.calculateTaxes(netPremium);


    }


}
