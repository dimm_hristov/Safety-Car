package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.repositories.contracts.CarRepository;
import com.tinqin.safetycar.repositories.contracts.UserRepository;
import com.tinqin.safetycar.services.CarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.tinqin.safetycar.helpers.CarBrandFactory.createCar;

@ExtendWith(MockitoExtension.class)
public class CarServiceTest {

    @InjectMocks
    CarServiceImpl carService;

    @Mock
    CarRepository mockCarRepository;

    @Mock
    UserRepository mockUserRepository;

    private List<Car> testList;
    private Car testCar;
    private User testUser;
    @BeforeEach
    public void setup(){
        testUser=new User();
        testCar=createCar();
        testList=new ArrayList<>();
    }

    @Test
    public void getAll_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockCarRepository.getAll())
                .thenReturn(testList);
        //Act
        testList.add(testCar);
        List<Car>actual=carService.getAll();
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).getAll();
        Assertions.assertEquals(actual,testList);
    }


    @Test
    public void getAllCarByUser_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockCarRepository.getAll())
                .thenReturn(testList);
        //Act
        List<Car>actual=carService.getAllCarsByUser(testUser.getId());
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).getAll();
        Assertions.assertEquals(actual,testList);
    }


    @Test
    public void create_ShouldCreateCar_IfCarDoesntExist() {
        //Arrange
       Mockito.when(mockCarRepository.existById(Mockito.anyInt())).thenReturn(false);
        //Act
        carService.create(testCar);
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).create(testCar);
    }


    @Test
    public void create_ShouldThrow_IfCarExist() {
        //Arrange
        Mockito.when(mockCarRepository.existById(Mockito.anyInt())).thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () ->carService.create(testCar));
        Mockito.verify(mockCarRepository,
                Mockito.times(0)).create(testCar);
    }

    @Test
    public void update_ShouldUpdateCar_IfUserIsOwner() {
        //Arrange
       Mockito.when(mockCarRepository.checkUserIsOwner(testCar,testUser)).thenReturn(false);
        //Act
        testUser.setId(1);
        carService.update(testCar,testUser);
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).update(testCar);
    }

    @Test
    public void update_ShouldUpdateCar_IfUserIsNotOwner() {
        //Arrange
        Mockito.when(mockCarRepository.checkUserIsOwner(testCar,testUser)).thenReturn(true);

        //Act, Assert
        testUser.setId(2);
        Assertions.assertThrows(InvalidOperationException.class,
                ()->carService.update(testCar,testUser));
        Mockito.verify(mockCarRepository,
                Mockito.times(0)).update(testCar);
    }

    @Test
    public void delete_ShouldThrow_IfUserIsNotOwner() {
        //Arrange
        Mockito.when(mockCarRepository.getById(Mockito.anyInt())).thenReturn(testCar);
        //Act, Assert
        testUser.setId(2);
        Assertions.assertThrows(InvalidOperationException.class,
                () ->carService.delete(testCar.getOwner().getId(),testUser));

        Mockito.verify(mockCarRepository,
                Mockito.times(0)).delete(Mockito.anyInt());
    }

    @Test
    public void delete_ShouldDeleteCar_IfUserIsOwner() {
        //Arrange
        Mockito.when(mockCarRepository.getById(Mockito.anyInt())).thenReturn(testCar);
        //Act
        testUser.setId(1);
        carService.delete(testCar.getOwner().getId(),testUser);
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }

    @Test
    public void delete_ShouldDelete() {
        //Arrange
        Mockito.when(mockCarRepository.getById(Mockito.anyInt())).thenReturn(testCar);
//        Mockito.doNothing().when(mockCarRepository).delete(Mockito.anyInt());
        //Act
        testUser.setId(1);
        carService.delete(Mockito.anyInt(),testUser);
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }






    @Test
    public void getById_ShouldReturn_CarById() {
        //Arrange
        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(testCar);
        //Act
        carService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).getById(Mockito.anyInt());

    }

}
