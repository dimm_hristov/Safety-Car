package com.tinqin.safetycar.service;

import com.tinqin.safetycar.models.Region;
import com.tinqin.safetycar.repositories.contracts.RegionRepository;
import com.tinqin.safetycar.services.RegionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RegionServiceTest {

    @InjectMocks
    RegionServiceImpl regionService;

    @Mock
    RegionRepository mockRegionRepository;

    private List<Region> testList;

    @BeforeEach
    public void setup(){
        testList=new ArrayList<>();
    }

    @Test
    public void getAll_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockRegionRepository.getAll()).thenReturn(testList);

        //Act
        List<Region>actual=regionService.getAll();
        //Assert
        Assertions.assertEquals(actual,testList);

        Mockito.verify(mockRegionRepository,
                Mockito.times(1)).getAll();
    }
}
