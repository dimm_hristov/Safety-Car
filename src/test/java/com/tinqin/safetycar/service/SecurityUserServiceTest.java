package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.repositories.contracts.SecurityUserRepository;
import com.tinqin.safetycar.services.SecurityUserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.HelperFactory.createMockSecurityUser;

@ExtendWith(MockitoExtension.class)
public class SecurityUserServiceTest {

    @InjectMocks
    private SecurityUserServiceImpl securityUserService;

    @Mock
    private SecurityUserRepository mockSecurityUserRepository;

    private SecurityUser testSecureUser;
    private List<SecurityUser> testList;

    @BeforeEach
    public void setup(){
        testSecureUser=createMockSecurityUser();
        testList=new ArrayList<>();
    }
    @Test
    public void update_ShouldUpdate(){
        //Arrange
        Mockito.doNothing().when(mockSecurityUserRepository).update(testSecureUser);
        //Act
        securityUserService.update(testSecureUser);

        //Assert
        Mockito.verify(mockSecurityUserRepository,
                Mockito.times(1)).update(testSecureUser);

    }


    @Test
    public void getByUserName_ShouldReturnIfUserExist(){
        //Arrange
        Mockito.when(mockSecurityUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testSecureUser);
        //Act
        SecurityUser actual=securityUserService.getByUsername(Mockito.anyString());

        //Assert
        Mockito.verify(mockSecurityUserRepository,
                Mockito.times(1)).getByUsername(Mockito.anyString());

        Assertions.assertEquals(testSecureUser,actual);
    }
}
