package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.repositories.contracts.CarRepository;
import com.tinqin.safetycar.repositories.contracts.PolicyRepository;
import com.tinqin.safetycar.repositories.contracts.UserRepository;
import com.tinqin.safetycar.services.PolicyServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.tinqin.safetycar.helpers.PolicyFactory.*;

@ExtendWith(MockitoExtension.class)
public class PolicyServiceTest {

    @InjectMocks
    PolicyServiceImpl policyService;

    @Mock
    PolicyRepository mockPolicyRepository;

    @Mock
    CarRepository mockCarRepository;

    @Mock
    UserRepository mockUserRepository;

    private List<Policy> testList;
    private Policy testPolicy;
    private User testUser;
    private User testAdmin;
    private PolicyStatus status;

    @BeforeEach
    public void setup(){
        testUser=createUser();
        testAdmin=createAdmin();
        testPolicy=createPolicy();
        testList=new ArrayList<>();
        status=new PolicyStatus();
    }

    @Test
    public void getAll_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        List<Policy> actual= new ArrayList<>(policyService.getAll());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();

        Assertions.assertEquals(actual,testList);
    }

    @Test
    public void create_ShouldCreate(){
        //Arrange
        Mockito.doNothing().when(mockPolicyRepository).create(testPolicy);
        //Act
        policyService.create(testPolicy,Mockito.anyInt());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).create(testPolicy);
    }

    @Test
    public void update_ShouldUpdate_IfUserIsOwnerAndStatusIsPending(){
        //Arrange
        Mockito.doNothing().when(mockPolicyRepository).update(testPolicy);
        //Act
        status.setStatus("pending");
        testPolicy.setStatus(status);
        policyService.update(testPolicy,testUser);
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).update(testPolicy);
    }



    @Test
    public void update_ShouldThrow_IfPolicyIsNotPending_AndUserIsOwner_WithRoleUser(){
        //Act,Assert
        status.setStatus("accepted");
        testPolicy.setStatus(status);

        Assertions.assertThrows(InvalidOperationException.class,
                () ->policyService.update(testPolicy,testUser));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(0)).update(testPolicy);
    }

    @Test
    public void update_ShouldUpdate_IfUserIsAdmin(){
        //Assert
        Mockito.doNothing().when(mockPolicyRepository).update(testPolicy);
        //Act,Assert
        policyService.update(testPolicy,testAdmin);

        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).update(testPolicy);
    }


    @Test
    public void update_ShouldThrow_IfUserIsNotOwner_AndIsNotAdmin(){
        //Act,Assert
        testUser.setId(2);
        Assertions.assertThrows(InvalidOperationException.class,
                () ->policyService.update(testPolicy,testUser));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(0)).update(testPolicy);
    }




    @Test
    public void delete_ShouldDelete_IfUserIsCreatorOfPolicy(){
        //Arrange
        Mockito.when(mockPolicyRepository.getById(testPolicy.getPolicyCreator().getId()))
                .thenReturn(testPolicy);
        //Act
        testUser.setId(1);
        policyService.delete(testPolicy.getId(),testUser);
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }

    @Test
    public void delete_ShouldThrow_IfUserIsNotCreatorOfPolicy(){
        //Arrange
        Mockito.when(mockPolicyRepository.getById(testPolicy.getPolicyCreator().getId()))
                .thenReturn(testPolicy);

        //Act, Assert
        testUser.setId(2);
        Assertions.assertThrows(InvalidOperationException.class,
                ()->policyService.delete(testPolicy.getId(),testUser));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(0)).delete(Mockito.anyInt());
    }

    @Test
    public void getById_ShouldReturnPolicyById(){
        //Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(testPolicy);
        //Act
        policyService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void sortBySubmitDate_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockPolicyRepository.sortBySubmitDate()).thenReturn(testList);
        //Act
        List<Policy> actual= new ArrayList<>(policyService.sortBySubmitDate());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).sortBySubmitDate();

        Assertions.assertEquals(actual,testList);
    }

    @Test
    public void sortByEndDate_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockPolicyRepository.sortByEndDate()).thenReturn(testList);
        //Act
        List<Policy> actual= new ArrayList<>(policyService.sortByEndDate());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).sortByEndDate();

        Assertions.assertEquals(actual,testList);
    }

    @Test
    public void getAllPoliciesByUserId_ShouldReturn_Collection(){
        //Arrange
        List<User>testUsersList=new ArrayList<>();
        testUsersList.add(testUser);
        Mockito.when(mockUserRepository.getAll()).thenReturn(testUsersList);
        Mockito.when(mockPolicyRepository.getAllPoliciesByUserId(Mockito.anyInt())).thenReturn(testList);
        //Act
        policyService.getAllPoliciesByUserId(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAllPoliciesByUserId(Mockito.anyInt());
    }


    @Test
    public void getAllPoliciesByUserId_ShouldThrow_IfUserDoesntExist(){
        //Arrange
        testUser.setId(2);
        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () ->policyService.getAllPoliciesByUserId(testUser.getId()));

        Mockito.verify(mockPolicyRepository,
                Mockito.times(0)).getAllPoliciesByUserId(Mockito.anyInt());
    }

    @Test
    public void getAllPoliciesByUserId_ShouldReturn_EmptyCollection(){
        //Arrange
        List<User>testUsersList=new ArrayList<>();
        testUsersList.add(testUser);
        testList.add(testPolicy);
        Mockito.when(mockUserRepository.getAll()).thenReturn(testUsersList);
        Mockito.when(mockPolicyRepository.getAllPoliciesByUserId(Mockito.anyInt())).thenReturn(testList);
        //Act
        policyService.getAllPoliciesByUserId(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAllPoliciesByUserId(Mockito.anyInt());
    }

    @Test
    public void getAllPendingPoliciesByUser_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        status.setStatus("pending");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        policyService.getAllPendingPoliciesByUser(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllPendingPoliciesByUser_ShouldThrow_IfUserDoesntHavePendingPolicies(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () ->policyService.getAllPendingPoliciesByUser(testUser.getId()));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllPendingPolicies_ShouldReturn_Collection(){
        //Arrange
        status.setStatus("pending");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        policyService.getAllPendingPolicies();
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllRejectedPoliciesByUser_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        status.setStatus("rejected");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        policyService.getAllRejectedPoliciesByUser(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllRejectedPoliciesByUser_ShouldThrow_IfUserDoesntHaveRejectedPolicies(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () ->policyService.getAllRejectedPoliciesByUser(testUser.getId()));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllRejectedPolicies_ShouldReturn_Collection(){
        //Arrange
        status.setStatus("rejected");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        policyService.getAllRejectedPolicies();
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllExpiredPoliciesByUser_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        status.setStatus("expired");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        policyService.getAllExpiredPoliciesByUser(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllExpiredPoliciesByUser_ShouldThrow_IfUserDoesntHaveExpiredPolicies(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () ->policyService.getAllExpiredPoliciesByUser(testUser.getId()));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllExpiredPolicies_ShouldReturn_Collection(){
        //Arrange
        status.setStatus("expired");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        policyService.getAllExpiredPolicies();
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }


    @Test
    public void getAllAcceptedPoliciesByUser_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        status.setStatus("accepted");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        policyService.getAllAcceptedPoliciesByUser(testUser.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllAcceptedPoliciesByUser_ShouldThrow_IfUserDoesntHaveAcceptedPolicies(){
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(testUser);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () ->policyService.getAllAcceptedPoliciesByUser(testUser.getId()));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllAcceptedPolicies_ShouldReturn_Collection(){
        //Arrange
        status.setStatus("accepted");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        policyService.getAllAcceptedPolicies();
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void deleteAllPoliciesByCarId_ShouldDelete(){
        //Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        Car newCar=new Car();
        newCar.setId(1);
        status.setStatus("expired");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        policyService.deleteAllPoliciesByCarId(testPolicy.getId(),testUser);
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void deleteAllPoliciesByCarId_ShouldThrowIfCarIdDoesntMatch(){
        //Arrange
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);

        Car newCar=new Car();
        newCar.setId(2);
        status.setStatus("accepted");
        testPolicy.setStatus(status);
        testList.add(testPolicy);
        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () ->policyService.deleteAllPoliciesByCarId(testPolicy.getId(),testUser));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
    }


    @Test
    public void getPoliciesByCity_ShouldReturn_Collection(){
        //Arrange
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);
        //Act
        City newCity=new City();
        newCity.setId(1);
        List<Policy> actual=policyService.getPoliciesByCity(newCity.getId());
        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
        Assertions.assertEquals(actual,testList);
    }

    @Test
    public void getPoliciesByCity_ShouldThrow_IfThereIsNoPoliciesForCity(){
        //Arrange
        testList.add(testPolicy);
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(testList);


        //Act, Assert
        City newCity=new City();
        newCity.setId(2);

        Assertions.assertThrows(EntityNotFoundException.class,
                ()->policyService.getPoliciesByCity(newCity.getId()));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).getAll();
            }
}
