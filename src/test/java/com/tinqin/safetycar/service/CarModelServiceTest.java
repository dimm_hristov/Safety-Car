package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;
import com.tinqin.safetycar.repositories.contracts.CarBrandRepository;
import com.tinqin.safetycar.repositories.contracts.CarModelRepository;
import com.tinqin.safetycar.services.CarModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.tinqin.safetycar.helpers.CarModelsFactory.createModel;

@ExtendWith(MockitoExtension.class)
public class CarModelServiceTest {

    @InjectMocks
    CarModelServiceImpl carModelService;

    @Mock
    CarModelRepository mockCarModelRepository;

    @Mock
    CarBrandRepository mockCarBrandRepository;

    private List<CarModel> testList;
    private CarModel testModel;

    @BeforeEach
    public void setup(){
        testList=new ArrayList<>();
        testModel=createModel();
    }


    @Test
    public void create_ShouldCreate_IfModelDoesntExist(){
        //Arrange
        Mockito.when(mockCarModelRepository.existByModel(Mockito.anyString())).thenReturn(false);
        //Act
        carModelService.create(testModel);
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).create(testModel);
    }

    @Test
    public void create_ShouldThrow_IfModelExist(){
        //Arrange
        Mockito.when(mockCarModelRepository.existByModel(Mockito.anyString())).thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
        () -> carModelService.create(testModel));

        Mockito.verify(mockCarModelRepository,
                Mockito.times(0)).create(testModel);
    }

    @Test
    public void create_ShouldThrow_IfModelForBrand_DoesntExist(){
        //Arrange
        Mockito.when(mockCarBrandRepository.existByBrandId(Mockito.anyInt())).thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> carModelService.create(testModel));

        Mockito.verify(mockCarModelRepository,
                Mockito.times(0)).create(testModel);
    }

    @Test
    public void create_ShouldCreate_IfModelForBrand_Exist(){
        //Arrange
        Mockito.when(mockCarBrandRepository.existByBrandId(Mockito.anyInt())).thenReturn(false);

        //Act
        carModelService.create(testModel);

        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).create(testModel);
    }



    @Test
    public void getAll_ShouldReturn_Collection(){
        //Arrange
        testList.add(testModel);
        Mockito.when(mockCarModelRepository.getAll()).thenReturn(testList);

        //Act
        List<CarModel> actual=carModelService.getAll();

        //Assert
        Assertions.assertEquals(actual,testList);

        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).getAll();
    }


    @Test
    public void update_ShouldUpdateModel(){
        //Arrange
        Mockito.doNothing().when(mockCarModelRepository).update(testModel);

        //Act
        carModelService.update(testModel);
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).update(testModel);
    }

    @Test
    public void delete_ShouldDeleteModel(){
        //Arrange
        Mockito.doNothing().when(mockCarModelRepository).delete(Mockito.anyInt());

        //Act
        carModelService.delete(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }


    @Test
    public void getById_ShouldReturn_ModelById(){
        //Arrange
        Mockito.when(mockCarModelRepository.getById(Mockito.anyInt())).
                thenReturn(testModel);
        //Act
        carModelService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }


    @Test
    public void getByName_ShouldReturn_ModelByName(){
        //Arrange
        Mockito.when(mockCarModelRepository.getByName(testModel.getModel())).
                thenReturn(testModel);
        //Act
        carModelService.getByName(testModel.getModel());
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).getByName(testModel.getModel());
    }

    @Test
    public void getAllBrandsAndModelsInMap_ShouldReturnMap() {
        //Arrange
        List<CarBrand> brands = new ArrayList<>(mockCarBrandRepository.getAll());
        Set<String> getAllModelsByBrand = new HashSet<>(mockCarModelRepository.getAllModelsByBrand(testModel.getBrand().getId()));

        //Act
        Map<String, Set<String>> brandsAndModels = new TreeMap(carModelService.getAllBrandsAndModelsInMap());
        for (CarBrand brand: brands) {
            brandsAndModels.put(testModel.getBrand().getBrand(),getAllModelsByBrand);
        }
        //Assert
        Mockito.verify(mockCarModelRepository,
                Mockito.times(1)).getAllModelsByBrand(testModel.getBrand().getId());
        }
    }



