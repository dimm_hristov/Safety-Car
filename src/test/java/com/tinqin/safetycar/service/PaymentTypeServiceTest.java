package com.tinqin.safetycar.service;

import com.tinqin.safetycar.models.PaymentType;
import com.tinqin.safetycar.models.VehiclePhoto;
import com.tinqin.safetycar.repositories.contracts.PaymentTypeRepository;
import com.tinqin.safetycar.services.PaymentTypeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.HelperFactory.createType;

@ExtendWith(MockitoExtension.class)
public class PaymentTypeServiceTest {

    @InjectMocks
    PaymentTypeServiceImpl paymentTypeService;

    @Mock
    PaymentTypeRepository mockPaymentTypeRepository;

    private PaymentType testType=createType();
    private List<PaymentType> testList=new ArrayList<>();

    @Test
    public void getAll_ShouldReturn_Collections(){
        //Arrange
        Mockito.when(mockPaymentTypeRepository.getAll())
                .thenReturn(testList);

        //Act
        List<PaymentType> actual=paymentTypeService.getAll();

        //Assert
        Assertions.assertEquals(actual,testList);

        Mockito.verify(mockPaymentTypeRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_ShouldReturn_PaymentType(){
        //Arrange
        Mockito.when(mockPaymentTypeRepository.getById(Mockito.anyInt()))
                .thenReturn(testType);
        //Act
        paymentTypeService.getById(Mockito.anyInt());

        //Assert
        Mockito.verify(mockPaymentTypeRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }
}
