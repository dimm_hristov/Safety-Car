package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.PolicyStatus;
import com.tinqin.safetycar.repositories.contracts.PolicyStatusRepository;
import com.tinqin.safetycar.services.PolicyStatusServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.tinqin.safetycar.helpers.HelperFactory.createStatus;

@ExtendWith(MockitoExtension.class)
public class PolicyStatusServiceTest {

    @InjectMocks
    PolicyStatusServiceImpl policyStatusService;

    @Mock
    PolicyStatusRepository mockPolicyStatusRepository;

    private PolicyStatus testStatus=createStatus();

    @Test
    public void getById_ShouldReturn_PolicyStatus(){
        //Arrange
        Mockito.when(mockPolicyStatusRepository.getById(Mockito.anyInt()))
                .thenReturn(testStatus);
        //Act
        policyStatusService.getById(Mockito.anyInt());

        //Assert
        Mockito.verify(mockPolicyStatusRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }

}
