package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.repositories.contracts.CarBrandRepository;
import com.tinqin.safetycar.services.CarBrandServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.CarBrandFactory.createBrand;

@ExtendWith(MockitoExtension.class)
public class CarBrandServiceTest {

    @InjectMocks
    CarBrandServiceImpl carBrandService;

    @Mock
    CarBrandRepository mockCarBrandRepository;

    private List<CarBrand> testList;
    private CarBrand testBrand;

    @BeforeEach
    public void setup() {
        testBrand = createBrand();
        testList = new ArrayList<>();
    }

    @Test
    public void getAll_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockCarBrandRepository.getAll()).thenReturn(testList);
        //Act
        List<CarBrand> actual = carBrandService.getAll();
        //Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(1)).getAll();
        Assertions.assertEquals(actual, testList);
    }

    @Test
    public void create_ShouldCreateIfBrandDoesntExist() {
        //Arrange
        Mockito.when(mockCarBrandRepository.validateUniqueBrand(Mockito.anyString())).
                thenReturn(false);
        //Act
        carBrandService.create(testBrand);
        //Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(1)).create(testBrand);
    }

    @Test
    public void create_ShouldThrowIfBrandExist() {
        //Arrange
        Mockito.when(mockCarBrandRepository.validateUniqueBrand(Mockito.anyString()))
                .thenReturn(true);
        //Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> carBrandService.create(testBrand));

        Mockito.verify(mockCarBrandRepository,
                Mockito.times(0)).create(testBrand);
    }

    @Test
    public void update_ShouldUpdateCarBrand() {
        //Arrange
        Mockito.doNothing().when(mockCarBrandRepository).update(testBrand);
        //Act
        carBrandService.update(testBrand);
        //Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(1)).update(testBrand);
    }

    @Test
    public void delete_ShouldDeleteCarBrand() {
        //Arrange
        Mockito.doNothing().when(mockCarBrandRepository).delete(Mockito.anyInt());
        //Act
        carBrandService.delete(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }

    @Test
    public void getById_ShouldReturnIfBrandExist() {
        //Arrange
        Mockito.when(mockCarBrandRepository.getById(Mockito.anyInt())).
                thenReturn(testBrand);
        //Act
        carBrandService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(2)).getById(Mockito.anyInt());
    }

    @Test
    public void getById_ShouldThrowIfBrandDoesntExist() {
        //Arrange
        Mockito.when(mockCarBrandRepository.getById(Mockito.anyInt())).
                thenReturn(null);

        //Act,Assert
        Mockito.verify(mockCarBrandRepository,
                Mockito.times(0)).getById(Mockito.anyInt());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> carBrandService.getById(Mockito.anyInt()));

    }
}
