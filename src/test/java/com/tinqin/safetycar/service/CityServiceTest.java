package com.tinqin.safetycar.service;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.models.Region;
import com.tinqin.safetycar.repositories.contracts.CityRepository;
import com.tinqin.safetycar.services.CityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.HelperFactory.createCity;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {

    @InjectMocks
    CityServiceImpl cityService;

    @Mock
    CityRepository mockCityRepository;


    private List<City> testList;
    private City testCity;

    @BeforeEach
    public void setup(){
        testCity=createCity();
        testList=new ArrayList<>();
    }

    @Test
    public void getAll_ShouldReturn_Collection(){
        //Arrange
        Mockito.when(mockCityRepository.getAll()).thenReturn(testList);

        //Act
        List<City>actual=cityService.getAll();
        //Assert
        Assertions.assertEquals(actual,testList);

        Mockito.verify(mockCityRepository,
                Mockito.times(1)).getAll();
    }


    @Test
    public void getById_ShouldReturn_CityById(){
        //Arrange
        Mockito.when(mockCityRepository.getById(Mockito.anyInt()))
                .thenReturn(testCity);
        //Act
        cityService.getById(Mockito.anyInt());

        //Assert
        Mockito.verify(mockCityRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void getByRegionId_ShouldReturn_AllCitiesByRegion(){
        //Arrange
        Mockito.when(mockCityRepository.getByRegionId(Mockito.anyInt()))
                .thenReturn(testList);
        //Act
        cityService.getByRegionId(Mockito.anyInt());

        //Assert
        Mockito.verify(mockCityRepository,
                Mockito.times(1)).getByRegionId(Mockito.anyInt());
    }
}
