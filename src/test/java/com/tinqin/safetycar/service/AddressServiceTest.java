package com.tinqin.safetycar.service;


import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Address;
import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.models.Region;
import com.tinqin.safetycar.repositories.contracts.AddressRepository;
import com.tinqin.safetycar.repositories.contracts.CityRepository;
import com.tinqin.safetycar.services.AddressServiceImpl;
import com.tinqin.safetycar.services.contracts.CityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.AddressFactory.createAddress;


@ExtendWith(MockitoExtension.class)
public class AddressServiceTest {

    @InjectMocks
    AddressServiceImpl addressService;

    @Mock
    AddressRepository mockAddressRepository;

    @Mock
    CityService cityService;

    @Mock
    CityRepository cityRepository;

    private List<Address> testList = new ArrayList<>();
    private Address testAddress;


    @BeforeEach
    public void setup() {
        testAddress = createAddress();

    }

    @Test
    public void getAll_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockAddressRepository.getAll()).thenReturn(testList);
        //Act
        List<Address> actual = addressService.getAll();
        //Assert
        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).getAll();
        Assertions.assertEquals(actual, testList);
    }



    @Test
    public void create_ShouldCreateAddress() {
        //Arrange
        City city = new City();
        Mockito.when(cityService.getById(1)).thenReturn(city);
        // Mockito.when(cityRepository.getById(Mockito.anyInt())).thenReturn(city);

        //Act
        addressService.create("address", 1);
        //Assert
        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).create(Mockito.any());
    }

    @Test
    public void update_ShouldUpdateAddressIfExist() {
        //Arrange
        Mockito.when(mockAddressRepository.isAddressExist(testAddress))
                .thenReturn(false);
        //Act
        addressService.update(testAddress);
        //Assert
        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).update(testAddress);
    }

    @Test
    public void update_ShouldUThrow_IfAddressDoesntExist() {
        //Arrange
        Mockito.when(mockAddressRepository.isAddressExist(testAddress))
                .thenReturn(true);
        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
        ()->addressService.update(testAddress));

        Mockito.verify(mockAddressRepository,
                Mockito.times(0)).update(testAddress);
    }


    @Test
    public void delete_ShouldDeleteAddress() {
        //Arrange
        Mockito.doNothing().when(mockAddressRepository).delete(Mockito.anyInt());
        //Act
        addressService.delete(Mockito.anyInt());
        //Assert
        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).delete(Mockito.anyInt());
    }

    @Test
    public void getById_ShouldReturnAddressById() {
        //Arrange
        Mockito.when(mockAddressRepository.getById(Mockito.anyInt())).
                thenReturn(testAddress);
        //Act
        addressService.getById(Mockito.anyInt());
        //Assert
        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).getById(Mockito.anyInt());
    }


}
