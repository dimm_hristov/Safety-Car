package com.tinqin.safetycar.repositories.contracts;
import com.tinqin.safetycar.models.Address;
import java.util.List;

public interface AddressRepository {

    List<Address> getAll();

    void create(Address address);

    void update(Address address);

    void delete(int id);

    Address getById(int id);

    boolean isAddressExist(Address address);
}
