package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface SecurityUserRepository  {

   SecurityUser getByUsername(String username);


    void update(SecurityUser user);
}
