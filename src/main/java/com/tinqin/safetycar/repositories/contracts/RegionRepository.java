package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.Region;

import java.util.List;

public interface RegionRepository {
    List<Region> getAll();
}
