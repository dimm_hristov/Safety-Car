package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface CityRepository  {

    List<City> getAll();

    List<City> getByRegionId(int id);

    City getById(int id);
}
