package com.tinqin.safetycar.repositories.contracts;


import com.tinqin.safetycar.models.User;

import java.util.List;

public interface UserRepository {

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    List<User> filterByEmail(String email);

    List<User> getAllBusinessUsers();

    List<User> getAllPersonalUsers();
}
