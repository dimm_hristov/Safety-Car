package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.CalculationTable;

import java.util.List;

public interface CalculationTableRepository {

    void create(CalculationTable calculationTable);

    void update(CalculationTable calculationTable);

    void delete(int id);

    List<CalculationTable> getAll();

    CalculationTable getById(int id);

    double calculateBaseAmount(int cubicCapacity, int firstRegDate);
}
