package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.verification.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository {

    VerificationToken getVerificationToken(String token);

    void saveToken(VerificationToken token);
}
