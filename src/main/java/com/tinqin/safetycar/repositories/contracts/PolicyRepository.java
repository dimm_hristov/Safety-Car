package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.Policy;

import java.util.List;

public interface PolicyRepository {
    void create(Policy policy);

    void update(Policy policy);

    void delete(int id);

    List<Policy> getAll();

    Policy getById(int id);

    int getPolicyCreatorId(int policyId);

    List<Policy> getAllPoliciesByUserId(int id);

    List<Policy> sortBySubmitDate();

    List<Policy> sortByEndDate();
}
