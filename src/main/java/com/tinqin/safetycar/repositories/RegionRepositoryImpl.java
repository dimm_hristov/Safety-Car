package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.models.Region;
import com.tinqin.safetycar.repositories.contracts.RegionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RegionRepositoryImpl implements RegionRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RegionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Region> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Region> query = session.createQuery("from Region ", Region.class);
            return query.list();
        }
    }
}
