package com.tinqin.safetycar.repositories;


import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.repositories.contracts.SecurityUserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class SecurityUserRepositoryImpl implements SecurityUserRepository {
    public static final String USER_DOESNT_EXIST_ERR_MESS = "User doesn't exist";
    private final SessionFactory sessionFactory;

    @Autowired
    public SecurityUserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public SecurityUser getByUsername(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<SecurityUser> query = session.createQuery("from SecurityUser where userName =:email", SecurityUser.class);
            query.setParameter("email", email);
            List<SecurityUser> securityUsers = query.list();
            if (securityUsers.isEmpty()) {
                throw new EntityNotFoundException(USER_DOESNT_EXIST_ERR_MESS);
            }
            return securityUsers.get(0);
        }
    }

    @Override
    public void update(SecurityUser user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

}
