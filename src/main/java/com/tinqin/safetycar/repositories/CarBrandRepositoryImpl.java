package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.repositories.contracts.CarBrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarBrandRepositoryImpl implements CarBrandRepository {
    public static final String CAR_BRAND_NOT_FOUND_ERR_MESS = "Car brand with id %d not found";
    private final SessionFactory sessionFactory;

    @Autowired
    public CarBrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarBrand> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand", CarBrand.class);
            return query.list();
        }
    }

    @Override
    public void create(CarBrand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brand);
        }
    }

    @Override
    public void update(CarBrand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(brand);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
        CarBrand brandToDelete=session.get(CarBrand.class,id);

        if(brandToDelete==null){
            throw new EntityNotFoundException(String.format(CAR_BRAND_NOT_FOUND_ERR_MESS, id));
        }
        session.beginTransaction();
        session.delete(brandToDelete);
        session.getTransaction().commit();

        }
    }

    @Override
    public CarBrand getById(int id) {
        try(Session session=sessionFactory.openSession()){
            return session.get(CarBrand.class,id);
        }
    }

    @Override
    public boolean existByBrandId(int brandId) {
        return getAll().stream().allMatch(b -> b.getId() != brandId);
    }

    @Override
    public boolean validateUniqueBrand(String brand) {
        return getAll().stream()
                .anyMatch(b -> b.getBrand().equals(brand));
    }
}
