package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Address;
import com.tinqin.safetycar.repositories.contracts.AddressRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressRepositoryImpl implements AddressRepository {

    public static final String ADDRESS_NOT_FOUND_ERR_MESSAGE = "Address with id %d, not found.";
    private final SessionFactory sessionFactory;

    @Autowired
    public AddressRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Address> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Address> query = session.createQuery("from Address", Address.class);
            return query.list();
        }
    }

    @Override
    public void create(Address address) {
        try (Session session = sessionFactory.openSession()) {
            session.save(address);
        }
    }

    @Override
    public void update(Address address) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(address);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Address address = session.get(Address.class, id);
            if (address == null) {
                throw new EntityNotFoundException(String.format(ADDRESS_NOT_FOUND_ERR_MESSAGE, id));
            }
            session.beginTransaction();
            session.delete(address);
            session.getTransaction().commit();
        }

    }


    @Override
    public Address getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Address address = session.get(Address.class, id);
            if (address == null) {
                throw new EntityNotFoundException(String.format(ADDRESS_NOT_FOUND_ERR_MESSAGE, id));
            }
            return address;
        }
    }

    @Override
    public boolean isAddressExist(Address address) {
        return getAll().stream().noneMatch(a -> a.getId() == address.getId());
    }
}
