package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.PaymentType;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.repositories.contracts.PaymentTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PaymentTypeRepositoryImpl implements PaymentTypeRepository {
    public static final String PAYMENT_TYPE_DOESNT_EXIST_ERR_MESS = "paymentType with id %d doesn't exist";

    private final SessionFactory sessionFactory;

    @Autowired
    public PaymentTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PaymentType> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PaymentType> query = session.createQuery("from PaymentType order by id asc", PaymentType.class);
            return query.list();
        }
    }

    @Override
    public PaymentType getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PaymentType paymentType = session.get(PaymentType.class, id);
            if (paymentType == null) {
                throw new EntityNotFoundException(String.format(PAYMENT_TYPE_DOESNT_EXIST_ERR_MESS, id));
            }
            return paymentType;
        }
    }


}
