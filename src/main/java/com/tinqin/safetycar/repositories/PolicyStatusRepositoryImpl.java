package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.PolicyStatus;
import com.tinqin.safetycar.repositories.contracts.PolicyStatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PolicyStatusRepositoryImpl implements PolicyStatusRepository {
    public static final String POLICY_STATUS_DOESNT_EXIST_ERR_MESS = "PolicyStatus with id %d doesn't exist";
    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyStatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PolicyStatus getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PolicyStatus policyStatus = session.get(PolicyStatus.class, id);
            if (policyStatus == null) {
                throw new EntityNotFoundException(String.format(POLICY_STATUS_DOESNT_EXIST_ERR_MESS, id));
            }
            return policyStatus;
        }
    }
}
