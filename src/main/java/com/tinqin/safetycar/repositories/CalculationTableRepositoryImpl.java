package com.tinqin.safetycar.repositories;


import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.CalculationTable;
import com.tinqin.safetycar.repositories.contracts.CalculationTableRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CalculationTableRepositoryImpl implements CalculationTableRepository {
    public static final String CALCULATION_TABLE_ROW_NOT_FOUND_ERR_MESS = "Row with id %d, not found.";
    private final SessionFactory sessionFactory;

    @Autowired
    public CalculationTableRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(CalculationTable calculationTable) {
        try (Session session = sessionFactory.openSession()) {
            session.save(calculationTable);
        }
    }

    @Override
    public void update(CalculationTable calculationTable) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(calculationTable);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            CalculationTable calculationTable = session.get(CalculationTable.class, id);
            if (calculationTable == null) {
                throw new EntityNotFoundException(String.format(CALCULATION_TABLE_ROW_NOT_FOUND_ERR_MESS, id));
            }
            session.beginTransaction();
            session.delete(id);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<CalculationTable> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CalculationTable> query = session.createQuery("from CalculationTable", CalculationTable.class);
            return query.list();
        }
    }

    @Override
    public CalculationTable getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CalculationTable calculationTableRow = session.get(CalculationTable.class, id);
            if (calculationTableRow == null) {
                throw new EntityNotFoundException(String.format(CALCULATION_TABLE_ROW_NOT_FOUND_ERR_MESS, id));
            }
            return calculationTableRow;
        }
    }

    @Override
    public double calculateBaseAmount(int cubicCapacity, int firstRegDate) {
        if (firstRegDate > LocalDate.now().getYear()) {
            throw new InvalidOperationException(
                    String.format("You must enter a valid year.Not after %s", LocalDate.now().getYear()));
        }
        try (Session session = sessionFactory.openSession()) {
            Query<CalculationTable> query = session.createQuery
                    ("from CalculationTable where" +
                                    " (minCubicCapacity <= :cubicCapacity and maxCubicCapacity >= :cubicCapacity)",
                            CalculationTable.class);
            query.setParameter("cubicCapacity", cubicCapacity);
            List<CalculationTable> resultList = query.list();
            int carYears = calculateCarAge(firstRegDate);
            List<CalculationTable> resultListSingle = resultList.stream()
                    .filter(r -> carYears >= r.getMinAgeCar() && carYears <= r.getMaxAgeCar())
                    .collect(Collectors.toList());
            return resultListSingle.get(0).getBaseAmount();
        }
    }

    private int calculateCarAge(int carAge) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - carAge;
    }
}
