package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {

    public static final String USER_DOESNT_EXIST = "User %s doesn't exist";
    public static final String USER_ALREADY_DELETED = "User %s is already deleted";
    public static final String USER_WITH_USERNAME_DOESNT_EXIST = "User with username %s doesn't exist";
    public static final String USER_WITH_USERNAME_HAS_BEEN_DELETED = "User with username %s has been deleted";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            User userToDelete = session.get(User.class, id);
            if (userToDelete == null) {
                throw new EntityHasBeenDeletedException(String.format(USER_DOESNT_EXIST,
                        userToDelete.getEmail()));
            }
            if (userToDelete.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format(USER_ALREADY_DELETED,
                        userToDelete.getEmail()));
            }

            userToDelete.setDeleted(true);
            session.beginTransaction();
            session.update(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }


    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(String.format
                        (USER_WITH_USERNAME_DOESNT_EXIST, user.getEmail()));
            }
            if (user.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        (USER_WITH_USERNAME_HAS_BEEN_DELETED, user.getEmail()));
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email=:email", User.class);
            query.setParameter("email", email);
            List<User> users = query.list();
            User user = null;
            try {
                user = users.get(0);
            } catch (IndexOutOfBoundsException e) {
                throw new EntityNotFoundException(String.format(USER_WITH_USERNAME_DOESNT_EXIST, email));
            }
            return users.get(0);
        }
    }

    @Override
    public List<User> filterByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email=:email", User.class);
            query.setParameter("email", email);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<User> getAllBusinessUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where business = true", User.class);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<User> getAllPersonalUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where business = false", User.class);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }
}
