package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.repositories.contracts.CarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CarRepositoryImpl implements CarRepository {
    public static final String CAR_DOESNT_EXIST_ERR_MESS = "You can't change car that does not exist";
    public static final String CAR_NOT_PRESENT_ERR_MESS = "Car with id %d is not present";
    public static final String CAR_ALREADY_DELETED_ERR_MESS = "Car %s %s is already deleted";
    public static final String CAR_WITH_ID_NOT_FOUND = "Car with id:%d not found";
    public static final String CAR_WAS_DELETED_ERR_MESS = "Car with id:%d has been deleted";
    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.save(car);
        }
    }

    @Override
    public void update(Car car) {
        try (Session session = sessionFactory.openSession()) {
            if (session.get(Car.class, car.getId()) == null) {
                throw new EntityNotFoundException(CAR_DOESNT_EXIST_ERR_MESS);
            }
            session.beginTransaction();
            session.update(car);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car carToDelete = session.get(Car.class, id);
            if (carToDelete == null) {
                throw new EntityNotFoundException(String.format(CAR_NOT_PRESENT_ERR_MESS, id));
            }
            if (carToDelete.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format(CAR_ALREADY_DELETED_ERR_MESS,
                        carToDelete.getModel().getBrand().getBrand(), carToDelete.getModel().getModel()));
            }
            carToDelete.setDeleted(true);
            session.beginTransaction();
            session.update(carToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Car> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car", Car.class);
            return query.list().stream().filter(c -> !c.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public Car getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car car = session.get(Car.class, id);
            if (car == null) {
                throw new EntityNotFoundException(String.format(CAR_WITH_ID_NOT_FOUND, id));
            }
            if (car.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format(CAR_WAS_DELETED_ERR_MESS, car.getId()));
            }
            return car;
        }
    }

    @Override
    public boolean existById(int carId) {
        return getAll().stream().anyMatch(c -> c.getId() == carId);
    }

    @Override
    public boolean checkUserIsOwner(Car car, User user) {
        return car.getOwner().getId() != user.getId();
    }


}
