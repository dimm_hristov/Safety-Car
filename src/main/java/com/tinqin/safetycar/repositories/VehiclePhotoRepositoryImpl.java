package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.VehiclePhoto;
import com.tinqin.safetycar.repositories.contracts.VehiclePhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehiclePhotoRepositoryImpl implements VehiclePhotoRepository {

    private final SessionFactory sessionFactory;

    public VehiclePhotoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(VehiclePhoto photo) {
        try (Session session = sessionFactory.openSession()) {
            session.save(photo);
        }
    }

    @Override
    public List<VehiclePhoto> getAllByCarId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<VehiclePhoto> query = session.createQuery("from VehiclePhoto where carId =:id", VehiclePhoto.class);
            query.setParameter("id", id);
            return query.list();

        }
    }


}
