package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.repositories.contracts.PolicyRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class PolicyRepositoryImpl implements PolicyRepository {
    public static final String POLICY_ALREADY_DELETED = "Policy with id %d is already deleted";
    public static final String POLICY_DOESNT_EXIST_ERR_MESS = "Policy with id %d doesn't exist";
    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policy);
        }
    }

    @Override
    public void update(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policy);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Policy policyToDelete = session.get(Policy.class, id);
            if (policyToDelete.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        (POLICY_ALREADY_DELETED, policyToDelete.getId()));
            }
            policyToDelete.setDeleted(true);
            session.beginTransaction();
            session.update(policyToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Policy> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy order by submitDate desc ", Policy.class);
            return query.list().stream().filter(q -> !q.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public Policy getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Policy policy = session.get(Policy.class, id);
            if (policy == null) {
                throw new EntityNotFoundException(String.format(POLICY_DOESNT_EXIST_ERR_MESS, id));
            }
            if (policy.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format("Access denied. Policy with id %d has been deleted", policy.getId()));
            }
            return policy;
        }
    }

    @Override
    public int getPolicyCreatorId(int policyId) {
        try (Session session = sessionFactory.openSession()) {
            Policy policy = getById(policyId);
            return policy.getCar().getOwner().getId();
        }
    }

    @Override
    public List<Policy> getAllPoliciesByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy where car.owner.id = :userId order by submitDate desc", Policy.class);
            query.setParameter("userId", userId);
            return query.list().stream().filter(p -> !p.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Policy> sortBySubmitDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy order by submitDate asc", Policy.class);
            return query.list().stream().filter(s -> !s.isDeleted()).collect(Collectors.toList());
        }
    }


    @Override
    public List<Policy> sortByEndDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy oreder order by endDate asc", Policy.class);
            return query.list().stream().filter(s -> !s.isDeleted()).collect(Collectors.toList());
        }
    }
}
