package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.models.verification.VerificationToken;
import com.tinqin.safetycar.repositories.contracts.VerificationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenRepositoryImpl implements VerificationTokenRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where token = :token",
                    VerificationToken.class);
            query.setParameter("token", token);
            return query.list().get(0);
        }
    }

    @Override
    public void saveToken(VerificationToken token) {
        try (Session session = sessionFactory.openSession()) {
            session.save(token);
        }
    }
}
