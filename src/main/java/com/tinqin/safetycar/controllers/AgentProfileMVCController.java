package com.tinqin.safetycar.controllers;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
@RequestMapping("/agent-panel")
public class AgentProfileMVCController {

    private final UserService userService;
    private final CarService carService;
    private final PolicyService policyService;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final CityService cityService;
    private final RegionService regionService;
    private final AddressService addressService;
    private final SecurityUserService securityUserService;

    @Autowired
    public AgentProfileMVCController(UserService userService,
                                     CarService carService,
                                     PolicyService policyService,
                                     CarBrandService carBrandService,
                                     CarModelService carModelService,
                                     CityService cityService,
                                     RegionService regionService,
                                     AddressService addressService, SecurityUserService securityUserService) {
        this.userService = userService;
        this.carService = carService;
        this.policyService = policyService;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.cityService = cityService;
        this.regionService = regionService;
        this.addressService = addressService;
        this.securityUserService = securityUserService;
    }

    @GetMapping
    public String getAgentPanelPage(Model model, Principal principal) {
        addAllAttributes(model, principal);
        return "agent-panel";
    }

    @GetMapping("/accept/{policyId}")
    public String acceptPolicy(@PathVariable int policyId, Principal principal) {
        SecurityUser user = securityUserService.getByUsername(principal.getName());
        try {
            userService.acceptPolicy(policyId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/agent-panel";
    }

    @GetMapping("/reject/{policyId}")
    public String rejectPolicy(@PathVariable int policyId, Principal principal) {
        SecurityUser user = securityUserService.getByUsername(principal.getName());
        try {
            userService.rejectPolicy(policyId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/agent-panel";
    }

    @GetMapping("/delete/{policyId}")
    public String deletePolicy(@PathVariable int policyId, Principal principal, RedirectAttributes redirAttr) {
        SecurityUser user = securityUserService.getByUsername(principal.getName());
        try {
            userService.deletePolicy(policyId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | InvalidOperationException e) {
            redirAttr.addFlashAttribute("errorMsg", e.getMessage());
            return "redirect:/agent-panel";
        }
        return "redirect:/agent-panel";
    }

    @GetMapping("/delete/user/{userId}")
    public String deleteUser(@PathVariable int userId, Principal principal, RedirectAttributes redirAttr) {
        SecurityUser user = securityUserService.getByUsername(principal.getName());
        try {
            userService.deleteUser(userId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | InvalidOperationException e) {
            redirAttr.addFlashAttribute("errorMsg", e.getMessage());
            return "redirect:/agent-panel";
        }
        return "redirect:/agent-panel";
    }

    private void addAllAttributes(Model model, Principal principal) {
        User user = mapFromPrincipalToUser(principal);
        model.addAttribute("principal", user);
        model.addAttribute("user", user);
        model.addAttribute("allPolicies", policyService.getAll());
        model.addAttribute("allPendingPolicies", policyService.getAllPendingPolicies());
        model.addAttribute("allAcceptedPolicies", policyService.getAllAcceptedPolicies());
        model.addAttribute("allRejectedPolicies", policyService.getAllRejectedPolicies());
        model.addAttribute("allExpiredPolicies", policyService.getAllExpiredPolicies());
        model.addAttribute("allUsers", userService.getAllClients());
    }

    private User mapFromPrincipalToUser(Principal principal) {
        User user = null;
        try {
            user = userService.getByEmail(principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }
}
