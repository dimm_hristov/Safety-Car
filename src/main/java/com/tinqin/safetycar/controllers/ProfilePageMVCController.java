package com.tinqin.safetycar.controllers;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.FileUploadService;
import com.tinqin.safetycar.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/profile-page")
public class ProfilePageMVCController {

    private final UserService userService;
    private final CarService carService;
    private final PolicyService policyService;
    private final CityService cityService;
    private final RegionService regionService;
    private final AddressService addressService;
    private final FileUploadService fileUploadService;
    private final VehiclePhotoService vehiclePhotoService;
    private final SecurityUserService securityUserService;

    @Autowired
    public ProfilePageMVCController(UserService userService, CarService carService, PolicyService policyService,
                                    CityService cityService, RegionService regionService, AddressService addressService,
                                    FileUploadService fileUploadService, VehiclePhotoService vehiclePhotoService,
                                    SecurityUserService securityUserService) {
        this.userService = userService;
        this.carService = carService;
        this.policyService = policyService;
        this.cityService = cityService;
        this.regionService = regionService;
        this.addressService = addressService;
        this.fileUploadService = fileUploadService;
        this.vehiclePhotoService = vehiclePhotoService;
        this.securityUserService = securityUserService;
    }

    @GetMapping
    public String getProfilePage(Model model, Principal principal) {

        User user = mapFromPrincipalToUser(principal);
        EditUserDto userToEdit = mapFromUserToEditUserDto(user);
        model.addAttribute("principal", user);
        model.addAttribute("editUserDto", userToEdit);
        model.addAttribute("cities", cityService.getAll());
        model.addAttribute("regions", regionService.getAll());
        List<Car> userCars = carService.getAllCarsByUser(user.getId());
        List<Policy> userPolicies = policyService.getAllPoliciesByUserId(user.getId());
        model.addAttribute("userPolicies", userPolicies);
        model.addAttribute("userCars", userCars);
        return "profile-page";
    }

    @PostMapping
    public String editUserProfile(@Valid @ModelAttribute EditUserDto editUserDto,
                                  BindingResult bindingResult, Model model, Principal principal) {
        User user = mapFromPrincipalToUser(principal);
        model.addAttribute("principal", user);
        model.addAttribute("editUserDto", editUserDto);
        if (bindingResult.hasErrors()) {
            model.addAttribute("cities", cityService.getAll());
            model.addAttribute("regions", regionService.getAll());
            return "profile-page";
        }
        user = editUserDtoToUser(editUserDto, user);
        try {
            userService.update(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "redirect:/profile-page";
    }

    @GetMapping("/{userId}")
    public String getSpecificProfilePage(@PathVariable int userId, Model model) {
        User user;
        try {
            user = userService.getById(userId);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        model.addAttribute("user", user);
        List<Car> userCars = carService.getAllCarsByUser(user.getId());
        List<Policy> userPolicies = policyService.getAllPoliciesByUserId(user.getId());
        model.addAttribute("userPolicies", userPolicies);
        model.addAttribute("userCars", userCars);
        return "review-user-profile";
    }

    @PostMapping("/delete/car/{carId}")
    public String deleteUserCar(@PathVariable int carId, Principal principal, RedirectAttributes redirAttr) {
        User user = mapFromPrincipalToUser(principal);
        try {
            policyService.deleteAllPoliciesByCarId(carId, user);
            carService.delete(carId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | InvalidOperationException e) {
            redirAttr.addFlashAttribute("errorMsg", e.getMessage());
        }
        return "redirect:/profile-page";
    }

    @PostMapping("/upload/photos/{carId}")
    public String uploadVehiclePhotos(@RequestParam("carphotos") MultipartFile[] multipartFiles,
                                      @PathVariable int carId) {
        fileUploadService.saveAllPhotos(multipartFiles, carId);
        return "redirect:/profile-page/gallery/" + carId;
    }

    @GetMapping("/gallery/{carId}")
    public String getVehiclePhotosPage(Model model, @PathVariable int carId) {
        List<VehiclePhoto> photos = vehiclePhotoService.getAllByCarId(carId);
        model.addAttribute("carPhotos", photos);
        return "gallery";
    }

    @GetMapping("/delete/policy/{policyId}")
    public String deletePolicy(@PathVariable int policyId, Principal principal, RedirectAttributes redirAttr) {
        SecurityUser user = securityUserService.getByUsername(principal.getName());
        try {
            userService.deletePolicyWithoutSendingEmail(policyId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | InvalidOperationException e) {
            redirAttr.addFlashAttribute("errorMsg", e.getMessage());
            return "redirect:/profile-page";
        }
        return "redirect:/profile-page";
    }

    private User mapFromPrincipalToUser(Principal principal) {
        User user = null;
        try {
            user = userService.getByEmail(principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }

    private EditUserDto mapFromUserToEditUserDto(User user) {
        EditUserDto editUserDto = new EditUserDto();
        editUserDto.setFirstName(user.getFirstName());
        editUserDto.setLastName(user.getLastName());
        editUserDto.setEmail(user.getEmail());
        editUserDto.setPhoneNumber(user.getPhoneNumber());
        editUserDto.setBirthday(user.getBirthday());
        if (user.getAddress() == null) {
            editUserDto.setCityId(null);
            editUserDto.setRegionId(null);
            editUserDto.setAddressDetails(null);
        } else {
            editUserDto.setCityId(user.getAddress().getCity().getId());
            editUserDto.setAddressDetails(user.getAddress().getAddress());
            editUserDto.setRegionId(user.getAddress().getCity().getRegion().getId());
        }
        return editUserDto;
    }

    public User editUserDtoToUser(EditUserDto dto, User user) {
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setBirthday(dto.getBirthday());
        Address address = createNewAddress(dto);
        user.setAddress(address);
        userService.update(user);
        return user;
    }

    private Address createNewAddress(EditUserDto dto) {
        Address address = new Address();
        City city = cityService.getById(dto.getCityId());
        address.setCity(city);
        address.setAddress(dto.getAddressDetails());
        addressService.create(address.getAddress(), address.getCity().getId());
        return address;
    }


}
