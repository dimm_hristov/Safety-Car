package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.contracts.CarModelService;
import com.tinqin.safetycar.services.contracts.CarService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/cars")
public class CarRestController {

    private final CarService carService;
    private final UserService userService;
    private final CarModelService carModelService;

    @Autowired
    public CarRestController(CarService carService, UserService userService, CarModelService carModelService) {
        this.carService = carService;
        this.userService = userService;
        this.carModelService = carModelService;
    }

    @GetMapping
    public List<Car> getAll() {
        return carService.getAll();
    }

    @GetMapping("/{id}")
    public Car getById(@PathVariable int id) {
        try {
            return carService.getById(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Car create(@Valid @RequestBody CarDto carDto, Principal principal) {
        try {
            User owner = userService.getByEmail(principal.getName());

            Car car = mapCarDtoToCar(carDto, owner, new Car());
            carService.create(car);
            return car;
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Car update(@RequestBody Car editedCar, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            Car car = carService.getById(editedCar.getId());

            car.setModel(editedCar.getModel());
            car.setFirstRegDate(editedCar.getFirstRegDate());
            car.setCubicCapacity(editedCar.getCubicCapacity());
            car.setCarRegCertificate(car.getCarRegCertificate());
            carService.update(car, user);
            return car;
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, Principal principal) {
        try {
            User carCreator = userService.getByEmail(principal.getName());
            carService.delete(id, carCreator);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    private Car mapCarDtoToCar(CarDto carDto, User owner, Car car) {
        car.setCubicCapacity(carDto.getCubicCapacity());
        CarModel carModel = carModelService.getById(carDto.getCarModelId());
        car.setModel(carModel);
        car.setFirstRegDate(carDto.getFirstRegDate());
        car.setOwner(owner);
        return car;
    }

}
