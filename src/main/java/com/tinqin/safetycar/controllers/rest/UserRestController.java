package com.tinqin.safetycar.controllers.rest;


import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CreateUserDto;
import com.tinqin.safetycar.models.Mapper;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.services.contracts.PolicyService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    public static final String CONFIRM_PASSWORD_ERR_MESSAGE = "Please enter the same password";
    private final UserService userService;
    private final UserDetailsManager userDetailsManager;
    private final PolicyService policyService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserRestController(UserService userService, UserDetailsManager userDetailsManager, PolicyService policyService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.policyService = policyService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/client/{filter}")
    public List<User> getAllPersonalOrBusinessUsers(@PathVariable String filter) {
        List<User> listToReturn = null;
        switch (filter) {
            case "personal":
                listToReturn = userService.getAllPersonalUsers();
                break;
            case "business":
                listToReturn = userService.getAllUsersByBusiness();
                break;
        }
        return listToReturn;
    }

    @PostMapping
    public User create(@Valid @RequestBody CreateUserDto createUserDto) {
        try {
            if (!createUserDto.getPassword().equals(createUserDto.getPasswordConfirmation())) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, CONFIRM_PASSWORD_ERR_MESSAGE);
            }
            createUserInUserTable(createUserDto);
            User user = Mapper.createUserDtoToUser(createUserDto);
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public User update(@Valid @RequestBody User user) {
        try {
            userService.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            userService.delete(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userId}")
    public User getByIdOrEmail(@PathVariable String userId) {
        User user;
        int id;
        try {
            try {
                id = Integer.parseInt(userId);
                user = userService.getById(id);
            } catch (NumberFormatException e) {
                user = userService.getByEmail(userId);
            }
            return user;
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/all/policies/{userId}")
    public List<Policy> getAllPoliciesByUserId(@PathVariable int userId) {
        try {
            return policyService.getAllPoliciesByUserId(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    private void createUserInUserTable(CreateUserDto createUserDto) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User
                        (createUserDto.getEmail(), passwordEncoder.encode(createUserDto.getPassword()), authorities);

        if (userDetailsManager.userExists(createUserDto.getEmail())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User with this email already exist");
        }
        userDetailsManager.createUser(newUser);
    }
}




