package com.tinqin.safetycar.controllers;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.EmailServiceImpl;
import com.tinqin.safetycar.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class IndexPageMVCController {
    private static final String SESSION_OBJECT = "MY_SESSION_OBJECT";

    private final CarModelService carModelService;

    @Autowired
    public IndexPageMVCController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @GetMapping
    public String showIndexPage(Model model) {
        model.addAttribute("brandsAndModels", carModelService.getAllBrandsAndModelsInMap());
        model.addAttribute("createPolicyDto", new CreatePolicyDto());
        return "index";
    }

    @PostMapping
    public String calculatePolicyPrice(@Valid @ModelAttribute CreatePolicyDto createPolicyDto,
                                       BindingResult bindingResult, HttpServletRequest request) {
        request.getSession().setAttribute(SESSION_OBJECT, createPolicyDto);
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult
                    .getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining(" ")));
            return "redirect:/";
        }
        return "redirect:/create-policy";
    }

    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
