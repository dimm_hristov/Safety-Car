package com.tinqin.safetycar.controllers;

import com.lowagie.text.DocumentException;
import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.FileUploadService;
import com.tinqin.safetycar.services.HtmlToPdfParser;
import com.tinqin.safetycar.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Controller
@RequestMapping("/create-policy")
public class CreatePolicyMVCController {

    private static final String SESSION_OBJECT = "MY_SESSION_OBJECT";
    public static final int POLICY_STATUS_PENDING = 1;

    private final UserService userService;
    private final CarService carService;
    private final PolicyService policyService;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final CalculationTableService calculationTableService;
    private final PaymentTypeService paymentTypeService;
    private final SecurityUserService securityUserService;
    private final PolicyStatusService policyStatusService;
    private final HtmlToPdfParser htmlToPdfParser;
    private final FileUploadService fileUploadService;

    @Autowired
    public CreatePolicyMVCController(UserService userService, CarService carService,
                                     PolicyService policyService, CarBrandService carBrandService,
                                     CarModelService carModelService, CalculationTableService calculationTableService,
                                     PaymentTypeService paymentTypeService, SecurityUserService securityUserService, PolicyStatusService policyStatusService, HtmlToPdfParser htmlToPdfParser, FileUploadService fileUploadService) {
        this.userService = userService;
        this.carService = carService;
        this.policyService = policyService;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.calculationTableService = calculationTableService;
        this.paymentTypeService = paymentTypeService;
        this.securityUserService = securityUserService;
        this.policyStatusService = policyStatusService;
        this.htmlToPdfParser = htmlToPdfParser;
        this.fileUploadService = fileUploadService;
    }

    @GetMapping
    public String getCreatePolicyPage(Model model, HttpServletRequest request) {
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);
        if (createPolicyDto == null) {
            createPolicyDto = new CreatePolicyDto();
        }
        double netPremium = getNetPremium(createPolicyDto);
        model.addAttribute("brandsAndModels", carModelService.getAllBrandsAndModelsInMap());
        model.addAttribute("createPolicyDto", createPolicyDto);
        model.addAttribute("netPremium", netPremium);
        model.addAttribute("carBrands", carBrandService.getAll());
        model.addAttribute("carModels", carModelService.getAll());
        model.addAttribute("paymentTypes", paymentTypeService.getAll());
        return "create-policy";
    }

    @PostMapping
    public String calculatePolicy(@Valid @ModelAttribute CreatePolicyDto createPolicyDto,
                                  BindingResult bindingResult, HttpServletRequest request, RedirectAttributes redirAttr,
                                  @RequestParam("image") MultipartFile multipartFile) throws IOException {
        if (bindingResult.hasErrors()) {
            return "redirect:/create-policy/plans";
        }
        try {
            fileUploadService.savePhotoToFileSystem(createPolicyDto, multipartFile);
        } catch (IOException e) {
            redirAttr.addFlashAttribute("errorMsg", e.getMessage());
        }
        request.getSession().setAttribute(SESSION_OBJECT, createPolicyDto);
        return "redirect:/create-policy/plans";
    }

    @GetMapping("/plans")
    public String getPricingPlanOptions(Model model, HttpServletRequest request) {
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);
        model.addAttribute("createPolicyDto", createPolicyDto);
        double netPremium = getNetPremium(createPolicyDto);
        double proPlan = calculationTableService.calculateProPlan(createPolicyDto);
        model.addAttribute("netPremium", netPremium);
        model.addAttribute("proPlan", proPlan);
        return "pricing-plans";
    }

    @GetMapping("/starter-plan/{plan}")
    public String setStarterPlan(HttpServletRequest request, @PathVariable String plan) {
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);
        switch (plan) {
            case "starter":
                createPolicyDto.setProPlan(false);
                break;
            case "pro":
                createPolicyDto.setProPlan(true);
                break;
        }
        double netPremium = getNetPremium(createPolicyDto);
        createPolicyDto.setTotalAmount(netPremium);
        request.getSession().setAttribute(SESSION_OBJECT, createPolicyDto);
        return "redirect:/create-policy/invoice";
    }

    @GetMapping("/invoice")
    public String getInvoicePage(HttpServletRequest request, Principal principal, Model model) {
        User owner = mapFromPrincipalToUser(principal);
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);
        model.addAttribute("createPolicyDto", createPolicyDto);
        model.addAttribute("user", owner);
        model.addAttribute("localDateTime", LocalDateTime.now());
        model.addAttribute("netPremium", getNetPremium(createPolicyDto));
        model.addAttribute("netPremiumWithTaxes", calculationTableService.calculateTaxes(getNetPremium(createPolicyDto)));
        double netPremiumPro = calculationTableService.calculateProPlan(createPolicyDto);
        model.addAttribute("netPremiumPro", netPremiumPro);
        model.addAttribute("netPremiumProWithTaxes", calculationTableService.calculateTaxes(netPremiumPro));
        model.addAttribute("paymentType", paymentTypeService.getById(createPolicyDto.getPaymentTypeId()).getPaymentType());
        return "policy-invoice";
    }

    @GetMapping("/submit")
    public String createPolicy(HttpServletRequest request, Principal principal) {
        User owner = mapFromPrincipalToUser(principal);
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);

        try {
            createPolicyFromDto(createPolicyDto, owner);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "redirect:/profile-page";

    }

    @GetMapping("/invoice/print")
    @ResponseBody
    public ResponseEntity<InputStreamResource> printInvoiceAsPdf(Principal principal, HttpServletRequest request)
            throws IOException, DocumentException {
        String outputFile = System.getProperty("user.home") + File.separator + "safety-car-invoice.pdf";

        Context context = createContextForPdf(request, principal);
        String html = htmlToPdfParser.parseThymeleafTemplate(context);
        htmlToPdfParser.generatePdfFromHtml(html, outputFile);

        File file2Upload = new File(outputFile);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=safety-car-invoice.pdf");
        System.out.println("The length of the file is : " + file2Upload.length());
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file2Upload));
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file2Upload.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }


    private Context createContextForPdf(HttpServletRequest request, Principal principal) {
        CreatePolicyDto createPolicyDto = (CreatePolicyDto) request.getSession().getAttribute(SESSION_OBJECT);
        User owner = mapFromPrincipalToUser(principal);

        Context context = new Context();
        context.setVariable("createPolicyDto", createPolicyDto);
        context.setVariable("user", owner);
        context.setVariable("localDateTime", LocalDateTime.now());
        context.setVariable("netPremium", String.format("%.2f", getNetPremium(createPolicyDto)));
        context.setVariable("netPremiumWithTaxes", String.format("%.2f", calculationTableService.calculateTaxes(getNetPremium(createPolicyDto))));
        double netPremiumPro = calculationTableService.calculateProPlan(createPolicyDto);
        context.setVariable("netPremiumPro", String.format("%.2f", netPremiumPro));
        context.setVariable("netPremiumProWithTaxes", String.format("%.2f", calculationTableService.calculateTaxes(netPremiumPro)));
        context.setVariable("paymentType", paymentTypeService.getById(createPolicyDto.getPaymentTypeId()).getPaymentType());
        String formattedDate = FormatDateNow();
        context.setVariable("dateNow", formattedDate);
        return context;
    }


    private String FormatDateNow() {
        LocalDateTime dateNow = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formatedDate = dateNow.format(formatter);
        return formatedDate;
    }


    private void createPolicyFromDto(CreatePolicyDto dto, User owner) {
        Car car = new Car();
        car.setModel(carModelService.getByName(dto.getCarModel()));
        car.setCubicCapacity(dto.getCubicCapacity());
        car.setFirstRegDate(dto.getCarFirstReg());
        car.setOwner(owner);
        car.setDeleted(false);
        car.setCarRegCertificate(dto.getCarRegCertificate());
        carService.create(car);

        Policy policy = new Policy();
        //to chech if the car is added
        policy.setCar(car);
        policy.setPaymentType(paymentTypeService.getById(dto.getPaymentTypeId()));
        policy.setSubmitDate(LocalDateTime.now());
        policy.setStartDate(dto.getStartDate());
        policy.setEndDate(dto.getStartDate().plusYears(1));
        policy.setStartTime(dto.getStartTime());
        policy.setStatus(policyStatusService.getById(POLICY_STATUS_PENDING));
        double totalAmount = calculationTableService.reCalculateIfProPlan(dto);
        policy.setTotalAmount(totalAmount);
        policy.setDeleted(false);
        policy.setProPlan(dto.isProPlan());
        policyService.create(policy, car.getId());
    }

    private double getNetPremium(CreatePolicyDto createPolicyDto) {
        double netPremium;
        try {
            netPremium = calculationTableService.calculateNetPremium(createPolicyDto);
        } catch (NullPointerException e) {
            netPremium = 0.0;
        }
        return netPremium;
    }

    private User mapFromPrincipalToUser(Principal principal) {
        User user = null;
        try {
            user = userService.getByEmail(principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }
}
