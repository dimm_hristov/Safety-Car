package com.tinqin.safetycar.services;


import com.tinqin.safetycar.models.CalculationTable;
import com.tinqin.safetycar.models.CreatePolicyDto;
import com.tinqin.safetycar.repositories.contracts.CalculationTableRepository;
import com.tinqin.safetycar.services.contracts.CalculationTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.Date;
import java.util.List;

@Service
public class CalculationTableServiceImpl implements CalculationTableService {
    public static final double ACCIDENT_COEFFICIENT = 0.2;
    public static final double DRIVER_AGE_COEFFICIENT = 0.05;
    public static final int AGE_BORDER = 25;
    public static final double PRO_PLAN_COEFFICIENT = 0.3;
    public static final double TAX_PERCENT = 0.1;

    private final CalculationTableRepository calculationTableRepository;

    @Autowired
    public CalculationTableServiceImpl(CalculationTableRepository calculationTableRepository) {
        this.calculationTableRepository = calculationTableRepository;
    }

    @Override
    public void create(CalculationTable calculationTable) {
        calculationTableRepository.create(calculationTable);
    }

    @Override
    public void update(CalculationTable calculationTable) {
        calculationTableRepository.update(calculationTable);
    }

    @Override
    public void delete(int id) {
        calculationTableRepository.delete(id);
    }

    @Override
    public List<CalculationTable> getAll() {
        return calculationTableRepository.getAll();
    }

    @Override
    public CalculationTable getById(int id) {
        return calculationTableRepository.getById(id);
    }

    @Override
    public double calculateNetPremium(CreatePolicyDto createPolicyDto) {
        int cubicCapacity = createPolicyDto.getCubicCapacity();
        int carAge = createPolicyDto.getCarFirstReg();
        LocalDate driverBirthday = createPolicyDto.getDriverBirthday();
        boolean hasAccidents = createPolicyDto.getAccidents();

        double baseAmount = calculationTableRepository.calculateBaseAmount(cubicCapacity, carAge);
        double netPremium = baseAmount;
        if (hasAccidents) {
            netPremium += baseAmount * ACCIDENT_COEFFICIENT;
        }

        int driverAge = calculateAge(driverBirthday);
        if (driverAge < AGE_BORDER) {
            netPremium += netPremium * DRIVER_AGE_COEFFICIENT;
        }
        return netPremium;
    }

    @Override
    public double reCalculateIfProPlan(CreatePolicyDto createPolicyDto) {
        double netPremium = calculateNetPremium(createPolicyDto);
        if (createPolicyDto.isProPlan()) {
            netPremium += netPremium * PRO_PLAN_COEFFICIENT;
        }
        return netPremium;
    }

    @Override
    public double calculateProPlan(CreatePolicyDto createPolicyDto) {
        double netPremium = calculateNetPremium(createPolicyDto);
        return (netPremium + netPremium * PRO_PLAN_COEFFICIENT);
    }

    @Override
    public double calculateTaxes(double netPremium) {
        netPremium = netPremium + netPremium * TAX_PERCENT;
        return netPremium;
    }

    private static int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = java.time.LocalDate.now();
        if (birthDate != null) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
}
