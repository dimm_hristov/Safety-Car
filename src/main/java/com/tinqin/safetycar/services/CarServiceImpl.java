package com.tinqin.safetycar.services;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.repositories.contracts.CarRepository;
import com.tinqin.safetycar.repositories.contracts.UserRepository;
import com.tinqin.safetycar.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    public static final String USER_NOT_ALLOWED_TO_EDIT_EMAIL_ERR_MESS = "User with email %s is not allowed to delete this car";
    public static final String CAR_ALREADY_EXIST = "This car is already created";
    public static final String USER_NOT_ALLOWED_TO_EDIT = "User %s is not allowed to edit this car";
    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public List<Car> getAll() {
        return carRepository.getAll();
    }

    @Override
    public List<Car> getAllCarsByUser(int userId) {
        List<Car> carsByUser = new ArrayList<>();
        try {
            carsByUser = getAll().stream().filter(c -> c.getOwner().getId() == userId).collect(Collectors.toList());
        } catch (NullPointerException e) {

        }
        return carsByUser;
    }

    @Override
    public void create(Car car) {
        checkIfCarExist(car.getId());
        carRepository.create(car);
    }

    @Override
    public void update(Car car, User user) {
        userIsOwner(car,user);
        carRepository.update(car);
    }

    @Override
    public void delete(int id, User user) {
        Car carToDelete = carRepository.getById(id);
        if (carToDelete.getOwner().getId() != user.getId()) {
            throw new InvalidOperationException(String.format
                    (USER_NOT_ALLOWED_TO_EDIT_EMAIL_ERR_MESS, user.getEmail()));
        }
        carRepository.delete(id);
    }

    @Override
    public Car getById(int id) {
        return carRepository.getById(id);
    }

    private void checkIfCarExist(int carId) {
        if (carRepository.existById(carId)) {
            throw new DuplicateEntityException(CAR_ALREADY_EXIST);
        }
    }

    private void userIsOwner(Car car, User user) {
        if (carRepository.checkUserIsOwner(car,user)){
            throw new InvalidOperationException(String.format(USER_NOT_ALLOWED_TO_EDIT, user.getEmail()));
        }
    }
}
