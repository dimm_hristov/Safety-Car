package com.tinqin.safetycar.services;


import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;
import com.tinqin.safetycar.repositories.contracts.CarBrandRepository;
import com.tinqin.safetycar.repositories.contracts.CarModelRepository;
import com.tinqin.safetycar.services.contracts.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CarModelServiceImpl implements CarModelService {

    public static final String CAR_MODEL_ALREADY_EXIST = "Car model with %s already exist!";
    public static final String BRAND_WITH_ID_DOESNT_EXIST = "Brand with id %d doesn't exist";
    private final CarModelRepository carModelRepository;
    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository,
                               CarBrandRepository carBrandRepository) {
        this.carModelRepository = carModelRepository;
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarModel> getAll() {
        return carModelRepository.getAll();
    }

    @Override
    public void create(CarModel model) {
        chekIfModelExist(model.getModel());
        chekIfModelForBrandExist(model.getBrand().getId());
        carModelRepository.create(model);
    }

    @Override
    public void update(CarModel model) {
        carModelRepository.update(model);
    }

    @Override
    public void delete(int id) {
        carModelRepository.delete(id);
    }

    @Override
    public CarModel getById(int id) {
        return carModelRepository.getById(id);
    }

    @Override
    public CarModel getByName(String name) {
        return carModelRepository.getByName(name);
    }

    @Override
    public Map<String, Set<String>> getAllBrandsAndModelsInMap() {
        List<CarBrand> brands = new ArrayList<>(carBrandRepository.getAll());
        Map<String, Set<String>> brandsAndModels = new TreeMap<>();
        for (CarBrand brand : brands) {
            brandsAndModels.put(brand.getBrand(), carModelRepository.getAllModelsByBrand(brand.getId()));
        }
        return brandsAndModels;
    }

    private void chekIfModelExist(String model) {
        if (carModelRepository.existByModel(model)) {
            throw new DuplicateEntityException
                    (String.format(CAR_MODEL_ALREADY_EXIST, model));
        }
    }

    private void chekIfModelForBrandExist(int brandId) {
        if (carBrandRepository.existByBrandId(brandId)) {
            throw new EntityNotFoundException(String.format(BRAND_WITH_ID_DOESNT_EXIST, brandId));
        }
    }
}
