package com.tinqin.safetycar.services;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.repositories.contracts.CarRepository;
import com.tinqin.safetycar.repositories.contracts.PolicyRepository;
import com.tinqin.safetycar.repositories.contracts.UserRepository;
import com.tinqin.safetycar.services.contracts.PolicyService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PolicyServiceImpl implements PolicyService {

    public static final String USER_NOT_ALLOWED_TO_EDIT_POLICY = "User %s is not allowed to edit this policy";
    public static final String CANT_EDIT_APPROVED_OR_EXPIRED_POLICIES_ERR_MESS = "You are not allowed to edit already approved or expired policies";
    public static final String NOT_ALLOWED_TO_DELETE_CAR = "User %s is not allowed to delete this car";
    public static final String USER_DOESNT_EXIST = "User with id %d doesn't exist";
    public static final String USER_DOESNT_HAVE_PENDING_POLICIES = "User doesn't have any pending policies";
    public static final String USER_DOESNT_HAVE_ANY_REJECTED_POLICIES = "User doesn't have any rejected policies";
    public static final String USER_DOESNT_HAVE_ANY_EXPIRED_POLICIES = "User doesn't have any expired policies";
    public static final String USER_DOESNT_HAVE_ANY_ACCEPTED_POLICIES = "User doesn't have any accepted policies";
    public static final String ACTIVE_POLICIES_ARE_AVAILABLE_ERR_MESS = "You have active polices on this car.Please contact your agent if you want to delete this car";
    public static final String NO_POLICIES_FOR_CITY_ERR_MESS = "There is no policies for this city.";
    private final PolicyRepository policyRepository;
    private final CarRepository carRepository;
    private final UserRepository userRepository;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository, CarRepository carRepository,
                             UserRepository userRepository) {
        this.policyRepository = policyRepository;
        this.carRepository = carRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Policy> getAll() {
        return policyRepository.getAll();
    }

    @Override
    public void create(Policy policy, int carId) {
        Car car = carRepository.getById(carId);
        policy.setCar(car);
        policyRepository.create(policy);
    }

    @Override
    public void update(Policy policy, User user) {
        //if user is not the owner and is not admin he can't update
        if (user.getId() != policy.getPolicyCreator().getId() && !user.isAdmin()) {
            throw new InvalidOperationException(
                    String.format(USER_NOT_ALLOWED_TO_EDIT_POLICY, user.getEmail()));
        }
        //user can update only pending policies
        if (user.getId() == policy.getPolicyCreator().getId() && !user.isAdmin()) {
            if (!policy.getStatus().getStatus().equalsIgnoreCase("pending")) {
                throw new InvalidOperationException(CANT_EDIT_APPROVED_OR_EXPIRED_POLICIES_ERR_MESS);
            }
            policyRepository.update(policy);
        }
        if (user.isAdmin()) {
            policyRepository.update(policy);
        }
    }

    @Override
    public void delete(int id, User user) {
        if (user.getId() != policyRepository.getById(id).getPolicyCreator().getId()) {
            throw new InvalidOperationException(String.format(NOT_ALLOWED_TO_DELETE_CAR, user.getEmail()));
        }
        policyRepository.delete(id);
    }

    @Override
    public Policy getById(int id) {
        return policyRepository.getById(id);
    }

    @Override
    public List<Policy> getAllPoliciesByUserId(int id) {
        if (userRepository.getAll().stream().anyMatch(u -> u.getId() == id)) {
            List<Policy> userPolicies = policyRepository.getAllPoliciesByUserId(id);
            if (userPolicies.isEmpty()) {
                return new ArrayList<>();
            }
            return userPolicies;
        }
        throw new EntityNotFoundException(String.format(USER_DOESNT_EXIST, id));
    }

    @Override
    public List<Policy> getAllPendingPoliciesByUser(int userId) {
        userRepository.getById(userId);
        List<Policy> policies = policyRepository.getAll()
                .stream()
                .filter(p -> p.getPolicyCreator().getId() == userId)
                .filter(p -> p.getStatus().getStatus().equals("pending"))
                .collect(Collectors.toList());
        if (policies.isEmpty()) {
            throw new EntityNotFoundException(USER_DOESNT_HAVE_PENDING_POLICIES);
        }
        return policies;
    }

    @Override
    public List<Policy> getAllPendingPolicies() {
        return policyRepository.getAll()
                .stream()
                .filter(p -> p.getStatus().getStatus().equals("pending"))
                .collect(Collectors.toList());
    }


    @Override
    public List<Policy> getAllRejectedPoliciesByUser(int userId) {
        userRepository.getById(userId);
        List<Policy> policies = policyRepository.getAll()
                .stream()
                .filter(p -> p.getPolicyCreator().getId() == userId)
                .filter(p -> p.getStatus().getStatus().equals("rejected"))
                .collect(Collectors.toList());
        if (policies.isEmpty()) {
            throw new EntityNotFoundException(USER_DOESNT_HAVE_ANY_REJECTED_POLICIES);
        }
        return policies;
    }

    @Override
    public List<Policy> getAllRejectedPolicies() {
        return policyRepository.getAll()
                .stream()
                .filter(p -> p.getStatus().getStatus().equals("rejected"))
                .collect(Collectors.toList());
    }

    @Override
    public List<Policy> getAllExpiredPoliciesByUser(int userId) {
        userRepository.getById(userId);
        List<Policy> policies = policyRepository.getAll()
                .stream()
                .filter(p -> p.getPolicyCreator().getId() == userId)
                .filter(p -> p.getStatus().getStatus().equals("expired"))
                .collect(Collectors.toList());
        if (policies.isEmpty()) {
            throw new EntityNotFoundException(USER_DOESNT_HAVE_ANY_EXPIRED_POLICIES);
        }
        return policies;
    }

    @Override
    public List<Policy> getAllExpiredPolicies() {
        return policyRepository.getAll()
                .stream()
                .filter(p -> p.getStatus().getStatus().equals("expired"))
                .collect(Collectors.toList());
    }

    @Override
    public List<Policy> getAllAcceptedPoliciesByUser(int userId) {
        userRepository.getById(userId);
        List<Policy> policies = policyRepository.getAll()
                .stream()
                .filter(p -> p.getPolicyCreator().getId() == userId)
                .filter(p -> p.getStatus().getStatus().equals("accepted"))
                .collect(Collectors.toList());
        if (policies.isEmpty()) {
            throw new EntityNotFoundException(USER_DOESNT_HAVE_ANY_ACCEPTED_POLICIES);
        }
        return policies;
    }

    @Override
    public List<Policy> getAllAcceptedPolicies() {
        return policyRepository.getAll()
                .stream()
                .filter(p -> p.getStatus().getStatus().equals("accepted"))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAllPoliciesByCarId(int carId, User user) {
        getAll().forEach(policy -> {
            if (policy.getCar().getId() == carId) {
                if (!policy.getStatus().getStatus().equalsIgnoreCase("accepted")) {
                    delete(policy.getId(), user);
                } else {
                    throw new InvalidOperationException(ACTIVE_POLICIES_ARE_AVAILABLE_ERR_MESS);
                }

            }
        });
    }

    @Override
    public List<Policy> getPoliciesByCity(int cityId) {
        List<Policy> policies = policyRepository.getAll()
                .stream()
                .filter(p -> p.getCar().getOwner().getAddress().getCity().getId() == cityId)
                .collect(Collectors.toList());
        if (policies.isEmpty()) {
            throw new EntityNotFoundException(NO_POLICIES_FOR_CITY_ERR_MESS);
        }
        return policies;
    }

    @Override
    public List<Policy> sortBySubmitDate() {
        return policyRepository.sortBySubmitDate();
    }


    @Override
    public List<Policy> sortByEndDate() {
        return policyRepository.sortByEndDate();
    }
}
