package com.tinqin.safetycar.services;

import com.tinqin.safetycar.exceptions.*;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.models.verification.VerificationToken;
import com.tinqin.safetycar.repositories.contracts.*;
import com.tinqin.safetycar.services.contracts.CarService;
import com.tinqin.safetycar.services.contracts.EmailService;
import com.tinqin.safetycar.services.contracts.PolicyStatusService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    public static final String EXPIRED_VERIFICATION_TOKEN_MESSAGE = "Your verification token has expired, please request a new one.";
    public static final int POLICY_STATUS_ACCEPTED = 2;
    public static final int POLICY_STATUS_REJECTED = 3;
    public static final String ERROR_MESSAGE_DELETING_POLICY = "You cant delete an accepted offer that is ongoing.";
    public static final String ACCEPTED = "accepted";
    public static final String ERROR_MESSAGE_DELETE_USER = "You cant delete user that is having active policies";
    public static final String EMAIL_SUBJECT_INFORMATION = "SafetyCar | Your policy has been reviewed by our agents.";
    public static final String USER_ALREADY_EXIST = "User with username %s already exist.";
    public static final String USER_DOESNT_EXIST = "User with username %s doesn't exist.";
    public static final String NOT_ALLOWED_TO_ACCEPT_POLICY = "You are not allowed to accept this policy. Only agents are allowed.";
    public static final String EMAIL_CONFIRMATION_MESSAGE = "Hello, Your policy query was reviewed by agent." +
            " We would like to inform you that your policy has been %s";

    private final UserRepository userRepository;
    private final PolicyRepository policyRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final SecurityUserRepository securityUserRepository;
    private final PolicyStatusService policyStatusService;
    private final EmailService emailService;
    private final CarService carService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PolicyRepository policyRepository,
                           VerificationTokenRepository verificationTokenRepository,
                           SecurityUserRepository securityUserRepository, PolicyStatusService policyStatusService,
                           EmailService emailService, CarService carService) {
        this.userRepository = userRepository;
        this.policyRepository = policyRepository;
        this.verificationTokenRepository = verificationTokenRepository;
        this.securityUserRepository = securityUserRepository;
        this.policyStatusService = policyStatusService;
        this.emailService = emailService;
        this.carService = carService;
    }

    @Override
    public void create(User user) {
        if (!userRepository.filterByEmail(user.getEmail()).isEmpty()) {
            throw new DuplicateEntityException(String.format(USER_ALREADY_EXIST, user.getEmail()));
        }
        userRepository.create(user);
    }

    @Override
    public void update(User user) {
        checkIfUserExist(user);
        userRepository.update(user);
    }

    @Override
    public void delete(int id) {
        checkIfUserExist(userRepository.getById(id));
        userRepository.delete(id);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<User> getAllClients() {
        return getAll().stream().filter(u -> !u.getAuthorities().contains("ROLE_ADMIN")).collect(Collectors.toList());
    }

    @Override
    public User getById(int id) {
        User user;
        user= userRepository.getById(id);
        if (user == null) {
            throw new EntityNotFoundException(String.format("User with id %d doesn't exist", id));
        }
        if (user.isDeleted()) {
            throw new EntityHasBeenDeletedException(String.format
                    ("User with username %s  has been deleted", user.getEmail()));
        }
        return user;
    }

    @Override
    public User getByEmail(String email) {
        User user = userRepository.getByEmail(email);
        if (user.isDeleted()) {
            throw new EntityHasBeenDeletedException(String.format
                    ("User with username %s has been deleted", user.getEmail()));
        }
        return user;
    }


    @Override
    public void acceptPolicy(int policyId, SecurityUser user) {
        Policy policyToUpdate = doesUserHaveRightsToEditPolicy(policyId, user);
        policyToUpdate.setStatus(policyStatusService.getById(POLICY_STATUS_ACCEPTED));
        policyToUpdate.setAgent(this.getByEmail(user.getUserName()));
        policyRepository.update(policyToUpdate);
        sendInformationEmailAboutChanges(policyToUpdate, "accepted");
    }


    @Override
    public void rejectPolicy(int policyId, SecurityUser user) {
        Policy policyToUpdate = doesUserHaveRightsToEditPolicy(policyId, user);
        policyToUpdate.setStatus(policyStatusService.getById(POLICY_STATUS_REJECTED));
        policyToUpdate.setAgent(this.getByEmail(user.getUserName()));
        policyRepository.update(policyToUpdate);
        sendInformationEmailAboutChanges(policyToUpdate,"rejected");
    }


    @Override
    public void deletePolicy(int policyId, SecurityUser user) {
        Policy policyToDelete = policyRepository.getById(policyId);
        if (policyToDelete.getStatus().getStatus().equalsIgnoreCase(ACCEPTED)) {
            throw new InvalidOperationException(ERROR_MESSAGE_DELETING_POLICY);
        }
        policyRepository.delete(policyId);
        sendInformationEmailAboutChanges(policyToDelete,"deleted");
    }

    @Override
    public void deletePolicyWithoutSendingEmail(int policyId, SecurityUser user) {
        Policy policyToDelete = policyRepository.getById(policyId);
        if (policyToDelete.getStatus().getStatus().equalsIgnoreCase(ACCEPTED)) {
            throw new InvalidOperationException(ERROR_MESSAGE_DELETING_POLICY);
        }
        policyRepository.delete(policyId);
    }

    @Override
    public void deleteUser(int userId, SecurityUser user) {
        User userToDelete = this.getById(userId);
        if (isAccepted(userId)) {
            throw new InvalidOperationException(ERROR_MESSAGE_DELETE_USER);
        }
        policyRepository.getAllPoliciesByUserId(userId).forEach(p -> policyRepository.delete(p.getId()));
        carService.getAllCarsByUser(userId).forEach(c -> carService.delete(c.getId(),userToDelete));
        userRepository.delete(userId);
        SecurityUser secUser = securityUserRepository.getByUsername(userToDelete.getEmail());
        secUser.setEnabled(false);
        securityUserRepository.update(secUser);
    }

    @Override
    public List<User> getAllUsersByBusiness() {
        return userRepository.getAllBusinessUsers();
    }

    @Override
    public List<User> getAllPersonalUsers() {
        return userRepository.getAllPersonalUsers();
    }

    @Override
    public void createVerificationToken(SecurityUser user, String token) {
        VerificationToken tokenEntity = new VerificationToken(token, user);
        verificationTokenRepository.saveToken(tokenEntity);
    }

    @Override
    public void enableRegisteredUser(SecurityUser user, VerificationToken verificationToken) {
        Date tokenExpiryDate = verificationTokenRepository.getVerificationToken(verificationToken.getToken()).getExpiryDate();
        Date now = new Date();
        if (tokenExpiryDate.before(now)) {
            throw new ExpiredTokenException(EXPIRED_VERIFICATION_TOKEN_MESSAGE);
        }
        user.setEnabled(true);
        securityUserRepository.update(user);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return verificationTokenRepository.getVerificationToken(token);
    }

    private void checkIfUserExist(User user) {
        if (userRepository.getById(user.getId()) == null) {
            throw new EntityNotFoundException(String.format(USER_DOESNT_EXIST, user.getEmail()));
        }
    }

    private Policy doesUserHaveRightsToEditPolicy(int policyId, SecurityUser user) {
        Policy policyToUpdate = policyRepository.getById(policyId);
        if (!user.isAdmin()) {
            throw new InvalidOperationException(NOT_ALLOWED_TO_ACCEPT_POLICY);
        }
        return policyToUpdate;
    }

    private void sendInformationEmailAboutChanges(Policy policy,String acceptedRejected) {
        String to = policy.getCar().getOwner().getEmail();
        String subject = EMAIL_SUBJECT_INFORMATION;
        String message =String.format(EMAIL_CONFIRMATION_MESSAGE,acceptedRejected);
        emailService.sendSimpleMessage(to,subject,message);
    }

    private boolean isAccepted(int userId) {
        return policyRepository.getAllPoliciesByUserId(userId)
                .stream()
                .anyMatch(p -> p.getStatus().getStatus().equals("accepted"));
    }
}

