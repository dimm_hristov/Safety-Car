package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.repositories.contracts.CityRepository;
import com.tinqin.safetycar.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> getAll() {
        return cityRepository.getAll();
    }

    @Override
    public List<City> getByRegionId(int id) {
        return cityRepository.getByRegionId(id);
    }

    @Override
    public City getById(int id) {
        return cityRepository.getById(id);
    }
}
