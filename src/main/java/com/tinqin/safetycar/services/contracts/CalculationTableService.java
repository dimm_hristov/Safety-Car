package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.CalculationTable;
import com.tinqin.safetycar.models.CreatePolicyDto;

import java.util.Date;
import java.util.List;

public interface CalculationTableService {

    void create(CalculationTable calculationTable);

    void update(CalculationTable calculationTable);

    void delete(int id);

    List<CalculationTable> getAll();

    CalculationTable getById(int id);

    double calculateNetPremium(CreatePolicyDto createPolicyDto);

    double reCalculateIfProPlan(CreatePolicyDto createPolicyDto);

    double calculateProPlan(CreatePolicyDto createPolicyDto);

    double calculateTaxes(double netPremium);
}
