package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Address;
import com.tinqin.safetycar.models.City;

import java.util.List;

public interface AddressService {

    List<Address> getAll();

    void create(String address, int cityId);

    void update(Address address);

    void delete(int id);

    Address getById(int id);
}
