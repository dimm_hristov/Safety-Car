package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.SecurityUser;

public interface SecurityUserService {

    SecurityUser getByUsername(String username);

    void update(SecurityUser user);
}
