package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Region;

import java.util.List;

public interface RegionService {

    List<Region> getAll();
}
