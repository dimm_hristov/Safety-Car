package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CarModelService {

    List<CarModel> getAll();

    void create(CarModel model);

    void update(CarModel model);

    void delete(int id);

    CarModel getById(int id);

    CarModel getByName(String name);



    Map<String, Set<String>> getAllBrandsAndModelsInMap();
}
