package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.verification.VerificationToken;

public interface IUserService {

    void createVerificationToken(SecurityUser user, String token);

    void enableRegisteredUser(SecurityUser user, VerificationToken verificationToken);

    VerificationToken getVerificationToken(String token);
}

