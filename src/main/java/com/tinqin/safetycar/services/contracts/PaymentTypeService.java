package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.PaymentType;

import java.util.List;

public interface PaymentTypeService {
    List<PaymentType> getAll();

    PaymentType getById(int id);
}
