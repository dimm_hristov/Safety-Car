package com.tinqin.safetycar.services.contracts;


public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);
}
