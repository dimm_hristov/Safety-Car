package com.tinqin.safetycar.services;



import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.Address;
import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.repositories.contracts.AddressRepository;
import com.tinqin.safetycar.services.contracts.AddressService;
import com.tinqin.safetycar.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    public static final String ADDRES_DOESNT_EXIST = "Address doesn't exist";
    private final AddressRepository addressRepository;
    private final CityService cityService;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, CityService cityService) {
        this.addressRepository = addressRepository;
        this.cityService = cityService;
    }

    @Override
    public List<Address> getAll() {
        return addressRepository.getAll();
    }

    @Override
    public void create(String address, int cityId) {
        Address newAddress = new Address();
        City city = cityService.getById(cityId);
        newAddress.setCity(city);
        newAddress.setAddress(address);
        addressRepository.create(newAddress);
    }

    @Override
    public void update(Address address) {
        checkIfAddressExist(address);
        addressRepository.update(address);
    }

    @Override
    public void delete(int id) {
        addressRepository.delete(id);
    }

    @Override
    public Address getById(int id) {
        return addressRepository.getById(id);
    }

    private void checkIfAddressExist(Address address) {
        if (addressRepository.isAddressExist(address)) {
            throw new EntityNotFoundException(ADDRES_DOESNT_EXIST);
        }
    }

}


