package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.Region;
import com.tinqin.safetycar.repositories.contracts.RegionRepository;
import com.tinqin.safetycar.services.contracts.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    @Autowired
    public RegionServiceImpl(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public List<Region> getAll() {
        return regionRepository.getAll();
    }
}
