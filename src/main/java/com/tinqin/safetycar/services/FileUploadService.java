package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.CreatePolicyDto;
import com.tinqin.safetycar.models.VehiclePhoto;
import com.tinqin.safetycar.services.contracts.VehiclePhotoService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

@Service
public class FileUploadService {

    private final VehiclePhotoService vehiclePhotoService;

    public FileUploadService(VehiclePhotoService vehiclePhotoService) {
        this.vehiclePhotoService = vehiclePhotoService;
    }

    public static void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }
    }

    public void savePhotoToFileSystem(CreatePolicyDto createPolicyDto, MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        createPolicyDto.setCarRegCertificate(fileName);
        String uploadDir = "src/main/resources/static/images/car-reg-certificates/";
        FileUploadService.saveFile(uploadDir, fileName, multipartFile);
    }

    public void saveAllPhotos(MultipartFile[] files, int carId) {
        Arrays.asList(files).stream().forEach(file ->{
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            savePhotoToDatabase(fileName, carId);
            String uploadDir = "src/main/resources/static/images/car-photos/";
            try {
                FileUploadService.saveFile(uploadDir, fileName, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void savePhotoToDatabase(String fileName, int carId) {
        VehiclePhoto photo = new VehiclePhoto(carId, fileName);
        vehiclePhotoService.create(photo);
    }
}
