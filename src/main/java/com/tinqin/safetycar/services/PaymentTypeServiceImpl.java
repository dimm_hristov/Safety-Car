package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.PaymentType;
import com.tinqin.safetycar.repositories.contracts.PaymentTypeRepository;
import com.tinqin.safetycar.services.contracts.PaymentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentTypeServiceImpl implements PaymentTypeService {

    private final PaymentTypeRepository paymentTypeRepository;

    @Autowired
    public PaymentTypeServiceImpl(PaymentTypeRepository paymentTypeRepository) {
        this.paymentTypeRepository = paymentTypeRepository;
    }

    @Override
    public List<PaymentType> getAll() {
        return paymentTypeRepository.getAll();
    }

    @Override
    public PaymentType getById(int id) {
        return paymentTypeRepository.getById(id);
    }
}
