package com.tinqin.safetycar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.Date;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private CarModel model;

    @Positive
    @Column(name = "cubic_capacity")
    private Integer cubicCapacity;

    @Column(name = "car_reg_certificate")
    private String carRegCertificate;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @Column(name = "deleted")
    private boolean deleted;

    @Positive
    @Column(name = "first_reg_date")
    private Integer firstRegDate;

    public Car() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getFirstRegDate() {
        return firstRegDate;
    }

    public void setFirstRegDate(Integer first_reg_date) {
        this.firstRegDate = first_reg_date;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getCarRegCertificate() {
        return carRegCertificate;
    }

    public void setCarRegCertificate(String carRegCertificate) {
        this.carRegCertificate = carRegCertificate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
