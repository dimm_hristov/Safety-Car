package com.tinqin.safetycar.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicle_photos")
public class VehiclePhoto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "car_id")
    private int carId;

    @Column(name = "photo")
    private String photo;

    public VehiclePhoto() {
    }

    public VehiclePhoto(int carId, String photo) {
        this.carId = carId;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
