package com.tinqin.safetycar.models;

import com.tinqin.safetycar.annotations.ValidPassword;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class CreateUserDto {

    @Email(message = "Please provide a valid email address", regexp = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$")
    private String email;

    @NotEmpty
    @ValidPassword

    private String password;

    @NotEmpty
    @ValidPassword
    private String passwordConfirmation;

    public CreateUserDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
