package com.tinqin.safetycar.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

public class CarDto {

    private int carModelId;

    @Positive
    private Integer cubicCapacity;

    @NotNull
    @Positive
    private Integer firstRegDate;

    public CarDto() {
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public Integer getFirstRegDate() {
        return firstRegDate;
    }

    public void setFirstRegDate(Integer firstRegDate) {
        this.firstRegDate = firstRegDate;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }
}
