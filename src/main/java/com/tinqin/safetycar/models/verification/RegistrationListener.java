package com.tinqin.safetycar.models.verification;

import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.services.contracts.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private IUserService service;
    private MessageSource messages;
    private JavaMailSender mailSender;

    @Autowired
    public RegistrationListener(IUserService service, @Qualifier("messageSource") MessageSource messages,
                                JavaMailSender mailSender) {
        this.service = service;
        this.messages = messages;
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        SecurityUser user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);

        String recipientAddress = user.getUserName();
        String subject = "Safety Car | Registration Confirmation";
        String confirmationUrl = event.getAppUrl() + "/confirm-registration?token=" + token;
        //String message = messages.getMessage("message.regSucc", null, event.getLocale());
        String message = "Hello, we are glad to have you on board.Please confirm your registration" +
                " by clicking the link bellow. If you haven't registered in our site, please ignore this message.";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + "\r\n" + "http://localhost:8080" + confirmationUrl);
        mailSender.send(email);
    }
}
