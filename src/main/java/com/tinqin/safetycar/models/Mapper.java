package com.tinqin.safetycar.models;

public class Mapper {

    public static User createUserDtoToUser(CreateUserDto createUserDto) {
        User user = new User();
        user.setEmail(createUserDto.getEmail());
        return user;
    }

    public static CarModel carModelDtoToCarModel(CarModelDto dto){
        CarModel model=new CarModel();
        model.setId(dto.getId());
        model.setModel(dto.getModel());
        return model;
    }
}
